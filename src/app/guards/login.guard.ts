import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppUser } from '../models/data/app-user';
import { appStoreSelectors } from '../stores/app-store/app-store.selectors';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  // user stream
  user$: Observable<AppUser> = this.store.select(appStoreSelectors.user);

  constructor(private store: Store, private router: Router) {}

  /**
   * can activate when user is in store
   */
  canActivate(): Observable<boolean> {
    return this.user$.pipe(
      map((user) => {
        const exists = !!user;

        if (!exists) {
          this.router.navigate(['/main/login']);
        }

        return exists;
      })
    );
  }
}
