import { TestBed } from '@angular/core/testing';

import { AccessHistoryGuard } from './access-history.guard';

describe('AccessHistoryGuard', () => {
  let guard: AccessHistoryGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AccessHistoryGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
