export class OptionItem<T = string> {
  // label of option
  label: string;
  // value of option
  value: T;

  constructor(label: string, value: T) {
    this.label = label;
    this.value = value;
  }
}
