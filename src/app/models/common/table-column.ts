export type TableColumnSortDirection = 'asc' | 'desc';

export class TableColumn {
  property: string;
  label: string;
  useSort: boolean;
  direction: TableColumnSortDirection = 'asc';

  constructor(property: string, label: string, useSort = false) {
    this.property = property;
    this.label = label;
    this.useSort = useSort;
  }

  /**
   * change sort direction
   */
  changeSortDirection() {
    switch (this.direction) {
      case 'asc': {
        this.direction = 'desc';
        break;
      }

      case 'desc': {
        this.direction = 'asc';
        break;
      }
    }
  }
}
