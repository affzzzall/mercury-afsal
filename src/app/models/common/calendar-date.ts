/**
 * wrapper class for date object
 */
export class CalendarDate {
  // date methods which will be used for creating instance via constructor
  private static setters: { [key: string]: keyof Date } = {
    year: 'setFullYear',
    month: 'setMonth',
    date: 'setDate',
    hour: 'setHours',
    minute: 'setMinutes',
    second: 'setSeconds'
  };

  // preserved date instance
  instance: Date;

  /**
   * create app date and apply date
   * @param date date object
   */
  static fromDate(date: Date) {
    const calendarDate = new CalendarDate();

    calendarDate.applyDate(date);

    return calendarDate;
  }

  /**
   * compare 2 app date instances
   * return 1 when origin is bigger
   * return -1 when origin is smaller
   * return 0 when origin and target is same
   * @param origin origin app date instance
   * @param target target app date instance
   * @param compareTime set true to compare with hour, minute, second
   */
  private static compareDates(origin: CalendarDate, target: CalendarDate, compareTime = false) {
    let time = origin.time;
    let targetTime = target.time;

    if (!compareTime) {
      const denominator = 1000 * 60 * 60 * 24;

      time = Math.floor(time / denominator);
      targetTime = Math.floor(targetTime / denominator);
    }

    if (time < targetTime) {
      return -1;
    } else if (time > targetTime) {
      return 1;
    } else {
      return 0;
    }
  }

  /**
   * CalendarDate constructor
   * @param year year value
   * @param month month value. no need to subtract 1 like existing Date object
   * @param date date value
   * @param hour hours value
   * @param minute minutes value
   * @param second seconds value
   */
  constructor(year?: number, month?: number, date?: number, hour?: number, minute?: number, second?: number) {
    const parameterMap = {
      year,
      month: month !== undefined && month !== null ? month - 1 : undefined,
      date,
      hour,
      minute,
      second
    };

    this.instance = new Date();

    // set date parameters to date instance
    Object.keys(parameterMap).forEach((key) => {
      const value = parameterMap[key];

      if (value !== undefined && value !== null) {
        this.instance[CalendarDate.setters[key]](value);
      }
    });
  }

  /**
   * apply date object to instance
   * @param date date object
   */
  applyDate(date: Date) {
    this.instance = date;
  }

  /**
   * return year
   */
  get year() {
    return this.instance.getFullYear();
  }

  /**
   * return month
   */
  get month() {
    return this.instance.getMonth() + 1;
  }

  /**
   * return date
   */
  get date() {
    return this.instance.getDate();
  }

  /**
   * return hour
   */
  get hour() {
    return this.instance.getHours();
  }

  /**
   * return minute
   */
  get minute() {
    return this.instance.getMinutes();
  }

  /**
   * return second
   */
  get second() {
    return this.instance.getSeconds();
  }

  /**
   * return millisecond
   */
  get millisecond() {
    return this.instance.getMilliseconds();
  }

  /**
   * return time
   */
  get time() {
    return this.instance.getTime();
  }

  /**
   * return day
   */
  get day() {
    return this.instance.getDay();
  }

  /**
   * compare with passed date and return true when the time of current instance is same or before than parameter
   * comparison will be done with year, month, date by default
   * use compareTime parameter to compare hour, minute, second
   * @param calendarDate app date instance
   * @param compareTime set true to compare with hour, minute, second
   */
  isBeforeThan(calendarDate: CalendarDate, compareTime = false) {
    if (calendarDate) {
      return CalendarDate.compareDates(this, calendarDate, compareTime) === -1;
    }
  }

  /**
   * compare with passed date and return true when the time of current instance is same or after than parameter
   * comparison will be done with year, month, date by default
   * use compareTime parameter to compare hour, minute, second
   * @param calendarDate app date instance
   * @param compareTime set true to compare with hour, minute, second
   */
  isAfterThan(calendarDate: CalendarDate, compareTime = false) {
    if (calendarDate) {
      return CalendarDate.compareDates(this, calendarDate, compareTime) === 1;
    }
  }

  /**
   * compare with passed date and return true when the time of current instance is same with the parameter
   * comparison will be done with year, month, date by default
   * use compareTime parameter to compare hour, minute, second
   * @param calendarDate app date instance
   * @param compareTime set true to compare with hour, minute, second
   */
  isSame(calendarDate: CalendarDate, compareTime = false) {
    if (calendarDate) {
      return CalendarDate.compareDates(this, calendarDate, compareTime) === 0;
    }
  }
}
