import { AppUserPermissions } from './app-user-permissions';
import { AppToolkit } from './app-toolkit';
import { AppActivityHistory } from './app-activity-history';
import { AppLocation } from './app-location';
import { AppKey } from './app-key';
import { AppAssociatedDevice } from './app-device';
import { AppAssociatedSpace } from './app-space';

export type AppAdminRole = 'Administrator' | 'Keyholder';
export type AppAdminState = 'Active' | 'Inactive' | 'Disabled' | 'Invited';
export type AppAdminStatus = 'Primary' | 'Backup';

export type AppUser = AppAdmin | AppKeyholder | AppAssociatedKeyholder;

export interface AppUserBase {
  id: string;
  name: string;
  role: AppAdminRole;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  avatar: string;
  location: AppLocation;
  state: AppAdminState;
  status: AppAdminStatus;
  question: string;
  answer: string;
  disabledReason: string;
  permissions: AppUserPermissions;
  createdAt: string;
  toolkit?: AppToolkit;
}

export interface AppAssociatedKeyholder extends AppUserBase {
  key: AppKey;
}

// admin
export interface AppAdmin extends AppUserBase {
  recentActivity: AppActivityHistory;
}

export interface AppKeyholder extends AppAssociatedKeyholder {
  pin: string;
  payload: string;
  description: string;
  associatedDevices: AppAssociatedDevice[];
  associatedSpaces: AppAssociatedSpace[];
}
