import { AppAccessHistoryErrorInfo } from './app-access-history-error-info';
import { AppLocation } from './app-location';
import { AppKeyholder } from './app-user';
import { AppAssociatedDevice } from './app-device';

export type AppAccessHistoryState = 'Success' | 'Error';

export interface AppAccessHistory {
  keyholder: AppKeyholder;
  device: AppAssociatedDevice;
  state: AppAccessHistoryState;
  location: AppLocation;
  errorInfo: AppAccessHistoryErrorInfo;
  createdAt: string;
  map: boolean;
}
