import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { AppStoreKey } from './app-store.actions';
import { appReducer } from './app-store.reducer';

@NgModule({
  declarations: [],
  imports: [CommonModule, StoreModule.forFeature(AppStoreKey, appReducer)]
})
export class AppStoreModule {}
