import { Injectable } from '@angular/core';
import { ApiBaseService } from './common/api-base.service';
import { HttpClient } from '@angular/common/http';
import { AppLocation } from '../models/data/app-location';

@Injectable({
  providedIn: 'root'
})
export class LocationService extends ApiBaseService {
  constructor(private http: HttpClient) {
    super();
  }

  /**
   * get locations
   */
  getLocations() {
    return this.http.get<AppLocation[]>(this.endpoint('/locations.json'));
  }
}
