import { TestBed } from '@angular/core/testing';

import { ActivityHistoryService } from './activity-history.service';

describe('ActivityService', () => {
  let service: ActivityHistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActivityHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
