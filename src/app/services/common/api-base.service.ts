import { environment } from '../../../environments/environment';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

const { api } = environment;

export class ApiBaseService {
  // host url
  protected host: string = api.host;
  // api delay for dev server
  protected delay: number = api.delay;

  constructor(path: string = '') {
    this.host += path;
  }

  /**
   * return endpoint with path
   * @param path path
   */
  protected endpoint(path: string = '') {
    return this.host + path;
  }

  /**
   * fake api call
   */
  protected fakeApi() {
    return of(true).pipe(delay(this.delay));
  }
}
