import { Injectable } from '@angular/core';
import { ApiBaseService } from './common/api-base.service';
import { HttpClient } from '@angular/common/http';
import { AppDevice } from '../models/data/app-device';
import { AppAccessHistory } from '../models/data/app-access-history';
import { catchError, delay, map } from 'rxjs/operators';
import { SortChanged } from '../components/dashboard/admins/admin-table/admin-table.component';
import { PageResponse } from '../models/common/page-response';
import { throwError } from 'rxjs';
import { AppUser } from '../models/data/app-user';
import { AppSpace } from '../models/data/app-space';

@Injectable({
  providedIn: 'root'
})
export class AccessHistoryService extends ApiBaseService {
  constructor(private http: HttpClient) {
    super('/access-histories');
  }

  /**
   * get access histories
   * @param location location id
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getAccessHistories(location: string, page: number, size: number, sorts?: SortChanged, filters?) {
    return this.http
      .get<AppAccessHistory[]>(this.endpoint('/get-access-histories.json'))
      .pipe(delay(this.delay))
      .pipe(
        map((histories) => {
          return new PageResponse(histories, page, size);
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * get access history by device
   * @param device device
   * @param page page
   * @param size size
   */
  getAccessHistoryByDevice(device: AppDevice, page: number, size: number) {
    return this.http
      .get<AppAccessHistory[]>(this.endpoint('/get-access-histories.json'))
      .pipe(delay(this.delay))
      .pipe(
        map((histories) => {
          return new PageResponse(histories, page, size);
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * get access history by user
   * @param user user
   * @param page page
   * @param size size
   */
  getAccessHistoryByUser(user: AppUser, page: number, size: number) {
    return this.http
      .get<AppAccessHistory[]>(this.endpoint('/get-access-histories.json'))
      .pipe(delay(this.delay))
      .pipe(
        map((histories) => {
          return new PageResponse(histories, page, size);
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * get access history by space
   * @param space space
   * @param page page
   * @param size size
   */
  getAccessHistoryAppSpace(space: AppSpace, page: number, size: number) {
    return this.http
      .get<AppAccessHistory[]>(this.endpoint('/get-access-histories.json'))
      .pipe(delay(this.delay))
      .pipe(
        map((histories) => {
          return new PageResponse(histories, page, size);
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }
}
