import { Injectable } from '@angular/core';
import { ApiBaseService } from './common/api-base.service';
import { catchError, delay, map } from 'rxjs/operators';
import { PageResponse } from '../models/common/page-response';
import { throwError } from 'rxjs';
import { SortChanged } from '../components/dashboard/admins/admin-table/admin-table.component';
import { AppUserPermissions } from '../models/data/app-user-permissions';
import { AppAdmin } from '../models/data/app-user';
import { AppAdminInviteFields } from '../components/dashboard/admins/side-modal-invite-admin/admin-invite-default/admin-invite-default.component';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService extends ApiBaseService {
  constructor(private http: HttpClient) {
    super('/users');
  }

  /**
   * get admins
   * @param location location id
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getAdmins(location: string, page: number, size: number, sorts?: SortChanged, filters?: { [key: string]: any }) {
    return this.http
      .get<AppAdmin[]>(this.endpoint('/get-admins.json'))
      .pipe(delay(this.delay))
      .pipe(map((admins) => new PageResponse(admins, page, size)))
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * update admin user
   * @param email email
   * @param name name
   * @param state state
   * @param disabledReason disabled reason
   * @param status status
   * @param permissions permissions
   */
  updateAdminUser(
    email: string,
    name: string,
    state: string,
    disabledReason: string,
    status: string,
    permissions: AppUserPermissions
  ) {
    return this.fakeApi().pipe(
      map(() => ({
        firstName: name.split(' ')[0],
        lastName: name.split(' ')[1] || null,
        email,
        state,
        disabledReason,
        status,
        permissions
      }))
    );
  }

  /**
   * send invite mail
   * @param data data
   * @param permissions permissions
   */
  sendInviteMail(data: AppAdminInviteFields, permissions: AppUserPermissions) {
    return this.fakeApi();
  }

  /**
   * delete admin user
   * @param user user to delete
   */
  deleteAdmin(user: AppAdmin) {
    return this.fakeApi();
  }
}
