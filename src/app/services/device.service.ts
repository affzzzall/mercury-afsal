import { Injectable } from '@angular/core';
import { ApiBaseService } from './common/api-base.service';
import { HttpClient } from '@angular/common/http';
import { SortChanged } from '../components/dashboard/admins/admin-table/admin-table.component';
import { catchError, delay, map } from 'rxjs/operators';
import { AppAssociatedDevice, AppDevice } from '../models/data/app-device';
import { PageResponse } from '../models/common/page-response';
import { throwError } from 'rxjs';
import { AppKeyholder } from '../models/data/app-user';

@Injectable({
  providedIn: 'root'
})
export class DeviceService extends ApiBaseService {
  constructor(private http: HttpClient) {
    super('/devices');
  }

  /**
   * get devices
   * @param location location id
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getDevices(location: string, page: number, size: number, sorts?: SortChanged, filters?) {
    return this.http
      .get<AppDevice[]>(this.endpoint('/get-devices.json'))
      .pipe(delay(this.delay))
      .pipe(
        map((devices) => {
          return new PageResponse(devices, page, size);
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }

  /**
   * get unassociated devices
   * @param sortBy sort by
   */
  getUnassociatedDevices(sortBy: string) {
    return this.http
      .get<AppAssociatedDevice[]>(this.endpoint('/get-unassociated-devices.json'))
      .pipe(delay(this.delay));
  }

  /**
   * add space associations
   */
  addSpaceAssociations() {
    return this.fakeApi();
  }

  /**
   * verify toolkit
   */
  verifyToolkit() {
    return this.fakeApi();
  }

  /**
   * update device
   * @param name name
   */
  updateDevice(name: string) {
    return this.fakeApi().pipe(
      map(() => ({
        name
      }))
    );
  }
}
