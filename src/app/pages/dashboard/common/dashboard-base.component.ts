import { OnDestroy, OnInit } from '@angular/core';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { Observable } from 'rxjs';
import { AppLocation } from '../../../models/data/app-location';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../../../stores/app-store/app-store.selectors';
import { SortChanged } from '../../../components/dashboard/admins/admin-table/admin-table.component';
import { PagingChangeEvent } from '../../../components/common/paginator/models/paging-change-event';
import {
  AppTableActionsViewType,
  AppTableActionsViewTypes
} from '../../../components/common/table-actions/table-actions.component';
import { AppDynamicFieldConfig } from '../../../components/common/dynamic-field/dynamic-field.component';
import { PageResponse } from '../../../models/common/page-response';
import { MessageService } from '../../../components/common/message/services/message.service';
import { AppStatus, AppStatusGroup } from '../../../models/data/app-status';
import { OptionItem } from '../../../models/common/option-item';

interface DashboardBaseSubscriptions extends SubscriptionKeys {
  location$: SubscriptionItem;
  statuses$: SubscriptionItem;
}

export class DashboardBaseComponent<T> implements OnInit, OnDestroy {
  // filters
  filters: { [key: string]: any } = {};
  // filter configs
  filterConfigs: AppDynamicFieldConfig[] = [];
  // sorts
  sorts: SortChanged = {};
  // page
  page = 0;
  // size
  size = 10;
  // total
  total = 0;
  // data
  data: T[] = [];
  // location stream
  location$: Observable<AppLocation> = this.store.select(appStoreSelectors.location);
  // location
  location: AppLocation;
  // loading state
  loading = false;
  // initialized state
  // it should be `false` before data loaded at least once
  initialized = false;
  // last updated date
  lastUpdate = new Date();
  // view type
  viewType: AppTableActionsViewType = 'list';
  // view types
  viewTypes: AppTableActionsViewTypes = {
    map: false,
    grid: false,
    list: false
  };
  // statuses stream
  statuses$: Observable<AppStatus[]> = this.store.select(appStoreSelectors.statuses);
  // inventory
  private privateInventory: SubscriptionInventory<DashboardBaseSubscriptions> = new SubscriptionInventory<
    DashboardBaseSubscriptions
  >();

  constructor(protected store: Store, protected messageService: MessageService) {}

  ngOnInit(): void {
    this.getLocation();
    this.getStatuses();
  }

  ngOnDestroy(): void {
    this.privateInventory.unSubscribeAll();
  }

  /**
   * get statuses
   */
  getStatuses() {
    const sub = this.statuses$.subscribe({
      next: (statuses) => {
        this.onStatusLoaded(statuses);
      }
    });

    this.privateInventory.store('statuses$', sub);
  }

  /**
   * should be overridden from the extended component
   * @param statuses loaded statuses
   */
  onStatusLoaded(statuses: AppStatus[]) {}

  /**
   * populate status filter
   * @param statuses statuses
   * @param group group
   */
  populateStatusFilter(statuses: AppStatus[], group: AppStatusGroup) {
    const options = statuses
      .filter((status) => status.group === group && status.use)
      .map((status) => new OptionItem(status.label, status.label));

    const statusFilter = this.filterConfigs.find((item) => item.label.toLowerCase() === 'status');

    if (statusFilter) {
      // add options to status filter
      statusFilter.options = [new OptionItem<string>('All', ''), ...options];
    }
  }

  /**
   * get location from store
   */
  getLocation() {
    this.privateInventory.unSubscribe('location$');

    const sub = this.location$.subscribe({
      next: (location) => {
        this.location = location;
        this.onLocationLoaded();
      }
    });

    this.privateInventory.store('location$', sub);
  }

  /**
   * call after location loaded
   * it can be overridden from the extended class
   */
  onLocationLoaded() {
    this.getData(0);
  }

  /**
   * get data
   * it should be overridden from the extended class
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getData(
    page: number = this.page,
    size: number = this.size,
    sorts: SortChanged = this.sorts,
    filters = this.filters
  ) {}

  /**
   * reload data according to page
   * it can be overridden from the extended class
   * @param event event
   */
  onPagingChanged(event: PagingChangeEvent) {
    this.getData(event.page, event.size);
  }

  /**
   * reload data according to sort
   * it can be overridden from the extended class
   * @param sorts changed sorts
   */
  onSortsChanged(sorts: SortChanged) {
    this.getData(this.page, this.size, sorts);
  }

  /**
   * filter applied
   * it can be overridden from the extended class
   * @param filters filters
   */
  onFilterApplied(filters: { [key: string]: any }) {
    this.filters = filters;
    this.getData(0);
  }

  /**
   * handle response
   * @param response page response
   * @param sorts sorts
   */
  handleResponse(response: PageResponse<T>, sorts: SortChanged) {
    this.page = response.page;
    this.size = response.size;
    this.total = response.total;
    this.data = response.data;
    this.sorts = sorts;
    this.loading = false;
    // set initialized state true
    this.initialized = true;
    // update last updated date
    this.lastUpdate = new Date();
  }

  /**
   * handle error
   * @param error error
   */
  handleError(error: Error) {
    this.messageService.open('error', error.message);
    this.loading = false;
    // set initialized state true
    this.initialized = true;
  }
}
