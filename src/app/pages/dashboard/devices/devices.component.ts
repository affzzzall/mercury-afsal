import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DashboardBaseComponent } from '../common/dashboard-base.component';
import { Store } from '@ngrx/store';
import { MessageService } from '../../../components/common/message/services/message.service';
import { AppDevice } from '../../../models/data/app-device';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { DeviceService } from '../../../services/device.service';
import { SortChanged } from '../../../components/dashboard/admins/admin-table/admin-table.component';
import { OptionItem } from '../../../models/common/option-item';
import {
  DEVICE_ACTIVE_STATE,
  DEVICE_DISABLED_STATE,
  DEVICE_ERROR_STATE,
  DEVICE_INACTIVE_STATE,
  DEVICE_LOW_BATTERY_STATE,
  DEVICE_TAMPERED_STATE,
  DEVICE_UNREGISTERED_STATE
} from '../../../utils/constants.util';
import { AppStatus } from '../../../models/data/app-status';
import { DeviceTableComponent } from '../../../components/dashboard/devices/device-table/device-table.component';
import { Observable } from 'rxjs';
import { appStoreSelectors } from '../../../stores/app-store/app-store.selectors';

interface DevicesSubscriptions extends SubscriptionKeys {
  getDevices: SubscriptionItem;
}

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['../common/dashboard-base.component.scss', './devices.component.scss']
})
export class DevicesComponent extends DashboardBaseComponent<AppDevice> implements OnInit, OnDestroy {
  // device table
  @ViewChild(DeviceTableComponent) deviceTable: DeviceTableComponent;
  // register device visible state
  registerDeviceVisible = false;
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.deviceEditPermission);
  // inventory
  private inventory: SubscriptionInventory<DevicesSubscriptions> = new SubscriptionInventory<DevicesSubscriptions>();

  constructor(protected store: Store, protected messageService: MessageService, private deviceService: DeviceService) {
    super(store, messageService);

    // init view types
    this.viewTypes = {
      map: true,
      grid: true,
      list: true
    };

    // filter configs
    this.filterConfigs = [
      {
        label: 'SN',
        type: 'input',
        width: 107,
        mobilefield: 48
      },
      {
        label: 'name',
        type: 'input',
        width: 137,
        mobilefield: 48
      },
      {
        label: 'state',
        type: 'select',
        options: [
          new OptionItem('All', ''),
          new OptionItem(DEVICE_ACTIVE_STATE, DEVICE_ACTIVE_STATE),
          new OptionItem(DEVICE_INACTIVE_STATE, DEVICE_INACTIVE_STATE),
          new OptionItem(DEVICE_DISABLED_STATE, DEVICE_DISABLED_STATE),
          new OptionItem(DEVICE_LOW_BATTERY_STATE, DEVICE_LOW_BATTERY_STATE),
          new OptionItem(DEVICE_UNREGISTERED_STATE, DEVICE_UNREGISTERED_STATE),
          new OptionItem(DEVICE_TAMPERED_STATE, DEVICE_TAMPERED_STATE),
          new OptionItem(DEVICE_ERROR_STATE, DEVICE_ERROR_STATE)
        ],
        width: 107,
        mobilefield: 48
      },
      {
        label: 'status',
        type: 'select',
        options: [],
        width: 107,
        mobilefield: 48
      }
    ];
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.inventory.unSubscribeAll();
  }

  /**
   * create status options
   * @param statuses statuses
   */
  onStatusLoaded(statuses: AppStatus[]) {
    this.populateStatusFilter(statuses, 'device');
  }

  /**
   * get devices
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getData(page: number = this.page, size: number = this.size, sorts: SortChanged = this.sorts, filters = this.filters) {
    this.inventory.unSubscribe('getDevices');

    // if global spinner is spinning,
    // do not show spinner for table
    if (this.initialized) {
      this.loading = true;
    }

    const sub = this.deviceService.getDevices(this.location.id, page, size, sorts).subscribe({
      next: (keyholders) => this.handleResponse(keyholders, sorts),
      error: (err) => this.handleError(err)
    });

    this.inventory.store('getDevices', sub);
  }

  /**
   * open register device
   */
  openRegisterDevice() {
    this.registerDeviceVisible = true;
  }

  /**
   * close register device
   */
  closeRegisterDevice() {
    this.registerDeviceVisible = false;
  }

  /**
   * open detail
   * @param device device
   */
  onOpenDetail(device: AppDevice) {
    if (this.deviceTable) {
      this.closeRegisterDevice();
      this.deviceTable.openDeviceInfo(device);
    }
  }
}
