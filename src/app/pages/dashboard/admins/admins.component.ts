import { Component, OnDestroy } from '@angular/core';
import { AdminService } from '../../../services/admin.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { Store } from '@ngrx/store';
import { AppAdmin } from '../../../models/data/app-user';
import { MessageService } from '../../../components/common/message/services/message.service';
import { SortChanged } from '../../../components/dashboard/admins/admin-table/admin-table.component';
import { DashboardBaseComponent } from '../common/dashboard-base.component';
import { OptionItem } from '../../../models/common/option-item';
import {
  ADMIN_ACTIVE_STATE,
  ADMIN_DISABLED_STATE,
  ADMIN_INACTIVE_STATE,
  ADMIN_INVITED_STATE
} from '../../../utils/constants.util';
import { AppStatus } from '../../../models/data/app-status';
import { Observable } from 'rxjs';
import { appStoreSelectors } from '../../../stores/app-store/app-store.selectors';

interface AdminsSubscriptions extends SubscriptionKeys {
  getAdmins: SubscriptionItem;
}

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['../common/dashboard-base.component.scss', './admins.component.scss']
})
export class AdminsComponent extends DashboardBaseComponent<AppAdmin> implements OnDestroy {
  // invite admin visible state
  inviteAdminVisible = false;
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.adminEditPermission);
  // inventory
  private inventory: SubscriptionInventory<AdminsSubscriptions> = new SubscriptionInventory<AdminsSubscriptions>();

  constructor(protected store: Store, protected messageService: MessageService, private adminService: AdminService) {
    super(store, messageService);

    // init sorts
    this.sorts = {
      id: 'asc',
      name: 'asc'
    };

    // init view types
    this.viewTypes = {
      map: false,
      grid: true,
      list: true
    };

    // init filter configs
    this.filterConfigs = [
      {
        label: 'Toolkit user',
        type: 'select',
        options: [
          new OptionItem('All', ''),
          new OptionItem('Enabled', 'Enabled'),
          new OptionItem('Disabled', 'Disabled')
        ],
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'ID',
        type: 'input',
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'Name',
        type: 'input',
        width: 117,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'Email ID',
        type: 'input',
        width: 117,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'State',
        type: 'select',
        options: [
          new OptionItem('All', ''),
          new OptionItem('Active', ADMIN_ACTIVE_STATE),
          new OptionItem('Inactive', ADMIN_INACTIVE_STATE),
          new OptionItem('Invited', ADMIN_INVITED_STATE),
          new OptionItem('Disabled', ADMIN_DISABLED_STATE)
        ],
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'Status',
        type: 'select',
        options: [new OptionItem('All', '')],
        width: 107,
        tabfield: 70,
        mobilefield: 48
      }
    ];
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.inventory.unSubscribeAll();
  }

  /**
   * create status options
   * @param statuses statuses
   */
  onStatusLoaded(statuses: AppStatus[]) {
    this.populateStatusFilter(statuses, 'admin');
  }

  /**
   * get admin data
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getData(page: number = this.page, size: number = this.size, sorts: SortChanged = this.sorts, filters = this.filters) {
    this.inventory.unSubscribe('getAdmins');

    // if global spinner is spinning,
    // do not show spinner for table
    if (this.initialized) {
      this.loading = true;
    }

    const sub = this.adminService.getAdmins(this.location.id, page, size, sorts).subscribe({
      next: (admins) => this.handleResponse(admins, sorts),
      error: (err) => this.handleError(err)
    });

    this.inventory.store('getAdmins', sub);
  }

  /**
   * open invite admin
   */
  openInviteAdmin() {
    this.inviteAdminVisible = true;
  }

  /**
   * close invite admin
   */
  closeInviteAdmin() {
    this.inviteAdminVisible = false;
  }
}
