import { Component, OnDestroy } from '@angular/core';
import { DashboardBaseComponent } from '../common/dashboard-base.component';
import { Store } from '@ngrx/store';
import { AppKeyholder } from '../../../models/data/app-user';
import { KeyholderService } from '../../../services/keyholder.service';
import { SortChanged } from '../../../components/dashboard/admins/admin-table/admin-table.component';
import { MessageService } from '../../../components/common/message/services/message.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { OptionItem } from '../../../models/common/option-item';
import {
  KEYHOLDER_ACTIVE_STATE,
  KEYHOLDER_DISABLED_STATE,
  KEYHOLDER_INACTIVE_STATE
} from '../../../utils/constants.util';
import { AppStatus } from '../../../models/data/app-status';
import { Observable } from 'rxjs';
import { appStoreSelectors } from '../../../stores/app-store/app-store.selectors';

interface KeyholdersSubscriptions extends SubscriptionKeys {
  getKeyholders: SubscriptionItem;
}

@Component({
  selector: 'app-keyholders',
  templateUrl: './keyholders.component.html',
  styleUrls: ['../common/dashboard-base.component.scss', './keyholders.component.scss']
})
export class KeyholdersComponent extends DashboardBaseComponent<AppKeyholder> implements OnDestroy {
  // new keyholder modal visible state
  newKeyholderVisible = false;
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.keyholderEditPermission);
  // inventory
  private inventory: SubscriptionInventory<KeyholdersSubscriptions> = new SubscriptionInventory<
    KeyholdersSubscriptions
  >();

  constructor(
    protected store: Store,
    protected messageService: MessageService,
    private keyholderService: KeyholderService
  ) {
    super(store, messageService);

    // init view types
    this.viewTypes = {
      map: false,
      grid: true,
      list: true
    };

    // init filter configs
    this.filterConfigs = [
      {
        label: 'Key sn',
        type: 'input',
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'Name',
        type: 'input',
        width: 117,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'Email ID',
        type: 'input',
        width: 117,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'Pin Code',
        type: 'input',
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'State',
        type: 'select',
        options: [
          new OptionItem('All', ''),
          new OptionItem(KEYHOLDER_ACTIVE_STATE, KEYHOLDER_ACTIVE_STATE),
          new OptionItem(KEYHOLDER_INACTIVE_STATE, KEYHOLDER_INACTIVE_STATE),
          new OptionItem(KEYHOLDER_DISABLED_STATE, KEYHOLDER_DISABLED_STATE)
        ],
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'Status',
        type: 'select',
        options: [],
        width: 107,
        tabfield: 70,
        mobilefield: 48
      }
    ];
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.inventory.unSubscribeAll();
  }

  /**
   * create status options
   * @param statuses statuses
   */
  onStatusLoaded(statuses: AppStatus[]) {
    this.populateStatusFilter(statuses, 'keyholder');
  }

  /**
   * get keyholders
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getData(page: number = this.page, size: number = this.size, sorts: SortChanged = this.sorts, filters = this.filters) {
    this.inventory.unSubscribe('getKeyholders');

    // if global spinner is spinning,
    // do not show spinner for table
    if (this.initialized) {
      this.loading = true;
    }

    const sub = this.keyholderService.getKeyholders(this.location.id, page, size, sorts).subscribe({
      next: (keyholders) => this.handleResponse(keyholders, sorts),
      error: (err) => this.handleError(err)
    });

    this.inventory.store('getKeyholders', sub);
  }

  /**
   * open new keyholder
   */
  openNewKeyholder() {
    this.newKeyholderVisible = true;
  }

  /**
   * close new keyholder
   */
  closeNewKeyholder() {
    this.newKeyholderVisible = false;
  }
}
