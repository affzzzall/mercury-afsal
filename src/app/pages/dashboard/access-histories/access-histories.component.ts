import { Component, OnDestroy } from '@angular/core';
import { DashboardBaseComponent } from '../common/dashboard-base.component';
import { AppAccessHistory } from '../../../models/data/app-access-history';
import { Store } from '@ngrx/store';
import { MessageService } from '../../../components/common/message/services/message.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { SortChanged } from '../../../components/dashboard/admins/admin-table/admin-table.component';
import { AccessHistoryService } from '../../../services/access-history.service';

interface AccessHistoriesSubscriptions extends SubscriptionKeys {
  getAccessHistories: SubscriptionItem;
}

@Component({
  selector: 'app-access-histories',
  templateUrl: './access-histories.component.html',
  styleUrls: ['../common/dashboard-base.component.scss', './access-histories.component.scss']
})
export class AccessHistoriesComponent extends DashboardBaseComponent<AppAccessHistory> implements OnDestroy {
  // inventory
  private inventory: SubscriptionInventory<AccessHistoriesSubscriptions> = new SubscriptionInventory<
    AccessHistoriesSubscriptions
  >();

  constructor(
    protected store: Store,
    protected messageService: MessageService,
    private accessHistoryService: AccessHistoryService
  ) {
    super(store, messageService);

    this.filterConfigs = [
      {
        label: 'key sn',
        type: 'input',
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'device sn',
        type: 'input',
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'device name',
        type: 'input',
        width: 117,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'start date',
        type: 'start-date',
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'end date',
        type: 'end-date',
        width: 107,
        tabfield: 70,
        mobilefield: 48
      },
      {
        label: 'operation code',
        type: 'input',
        width: 107,
        tabfield: 70,
        mobilefield: 48
      }
    ];
  }

  /**
   * get access histories
   * @param page page
   * @param size size
   * @param sorts sorts
   * @param filters filters
   */
  getData(
    page: number = this.page,
    size: number = this.size,
    sorts: SortChanged = this.sorts,
    filters: { [p: string]: any } = this.filters
  ) {
    this.inventory.unSubscribe('getAccessHistories');

    // if global spinner is spinning,
    // do not show spinner for table
    if (this.initialized) {
      this.loading = true;
    }

    const sub = this.accessHistoryService.getAccessHistories(this.location.id, page, size, sorts).subscribe({
      next: (admins) => this.handleResponse(admins, sorts),
      error: (err) => this.handleError(err)
    });

    this.inventory.store('getAccessHistories', sub);
  }
}
