import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessHistoriesComponent } from './access-histories.component';

describe('AccessHistoriesComponent', () => {
  let component: AccessHistoriesComponent;
  let fixture: ComponentFixture<AccessHistoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccessHistoriesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessHistoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
