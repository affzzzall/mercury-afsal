import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigComponent } from './config.component';
import { StatusDeviceComponent } from '../../../components/dashboard/config/status-device/status-device.component';
import { StatusDeviceModule } from '../../../components/dashboard/config/status-device/status-device.module';
import { StatusAdminComponent } from '../../../components/dashboard/config/status-admin/status-admin.component';
import { StatusKeyholderComponent } from '../../../components/dashboard/config/status-keyholder/status-keyholder.component';
import { StatusSpaceComponent } from '../../../components/dashboard/config/status-space/status-space.component';
import { StatusAdminModule } from '../../../components/dashboard/config/status-admin/status-admin.module';
import { StatusKeyholderModule } from '../../../components/dashboard/config/status-keyholder/status-keyholder.module';
import { StatusSpaceModule } from '../../../components/dashboard/config/status-space/status-space.module';

const routes: Routes = [
  {
    path: '',
    component: ConfigComponent,
    children: [
      {
        path: 'device',
        component: StatusDeviceComponent
      },
      {
        path: 'admin',
        component: StatusAdminComponent
      },
      {
        path: 'keyholder',
        component: StatusKeyholderComponent
      },
      {
        path: 'space',
        component: StatusSpaceComponent
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'device'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, StatusDeviceModule, StatusAdminModule, StatusKeyholderModule, StatusSpaceModule]
})
export class ConfigRoutingModule {}
