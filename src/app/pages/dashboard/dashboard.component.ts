import { Component, OnDestroy, OnInit } from '@angular/core';
import { LocationService } from '../../services/location.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../utils/subscribe.util';
import { Store } from '@ngrx/store';
import { appStoreActions } from '../../stores/app-store/app-store.actions';
import { StatusService } from '../../services/status.service';
import { ToggleService } from 'src/app/services/common/toggle.service';

interface DashboardSubscriptions extends SubscriptionKeys {
  locations: SubscriptionItem;
  statuses: SubscriptionItem;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  // inventory
  private inventory: SubscriptionInventory<DashboardSubscriptions> = new SubscriptionInventory<
    DashboardSubscriptions
  >();
  toggleNav = false;

  constructor(
    private store: Store,
    private locationService: LocationService,
    private statusService: StatusService,
    private navToogled: ToggleService
  ) {}

  ngOnInit(): void {
    this.getLocations();
    this.getStatuses();
    this.toggleNavigation();
    if (window.innerWidth > 767) {
      this.toggleNav = true;
    }
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * get locations
   */
  private getLocations() {
    const sub = this.locationService.getLocations().subscribe({
      next: (locations) => {
        // set locations
        this.store.dispatch(appStoreActions.setLocations({ locations }));
      }
    });

    this.inventory.store('locations', sub);
  }

  /**
   * get total statuses
   */
  private getStatuses() {
    const sub = this.statusService.getStatuses().subscribe({
      next: (statuses) => {
        // set statuses
        this.store.dispatch(appStoreActions.setStatuses({ statuses }));
      }
    });

    this.inventory.store('statuses', sub);
  }

  toggleNavigation() {
    this.navToogled.toggleNav.subscribe((val) => {
      this.toggleNav = val;
    });
  }
}
