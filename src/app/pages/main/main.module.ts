import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { LogoModule } from '../../components/common/logo/logo.module';
import { IconChevronRightDoubleModule } from '../../components/common/icons/icon-chevron-right-double/icon-chevron-right-double.module';
import { VideoPlayerModule } from '../../components/common/video-player/video-player.module';
import { MainFooterModule } from '../../components/main/common/main-footer/main-footer.module';
import { CookiePolicyModule } from '../../components/common/cookie-policy/cookie-policy.module';
import { FlatButtonModule } from 'src/app/components/common/buttons/flat-button/flat-button.module';
import { RouterModule } from '@angular/router';
import { LoginModule } from './login/login.module';

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    LogoModule,
    IconChevronRightDoubleModule,
    VideoPlayerModule,
    MainFooterModule,
    CookiePolicyModule,
    FlatButtonModule,
    RouterModule,
    LoginModule
  ]
})
export class MainModule {}
