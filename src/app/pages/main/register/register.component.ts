import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MainBaseComponent } from '../common/main-base.component';
import { UserService } from '../../../services/user.service';
import { MessageService } from '../../../components/common/message/services/message.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { Router } from '@angular/router';
import { ToggleService } from 'src/app/services/common/toggle.service';

interface RegisterSubscriptions extends SubscriptionKeys {
  register: SubscriptionItem;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends MainBaseComponent implements OnInit, OnDestroy {
  // captcha
  captcha: FormControl = new FormControl('', Validators.required);
  // question
  question: FormControl = new FormControl('', Validators.required);
  // answer
  answer: FormControl = new FormControl('', Validators.required);
  // agree state
  agree = false;
  // errors for captcha
  captchaErrors = {
    required: 'The code is required'
  };
  // errors for question
  questionErrors = {
    required: 'The secret question is required'
  };
  // errors for answer
  answerErrors = {
    required: 'The secret answer is required'
  };
  /**
   * form group
   */
  formGroup: FormGroup = new FormGroup({
    email: this.email,
    password: this.password,
    question: this.question,
    captcha: this.captcha,
    answer: this.answer
  });
  // loading state
  loading = false;
  // inventory
  private inventory: SubscriptionInventory<RegisterSubscriptions> = new SubscriptionInventory<RegisterSubscriptions>();

  constructor(
    private router: Router,
    private userService: UserService,
    private messageService: MessageService,
    private toggleService: ToggleService
  ) {
    super();
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * toggle agree state
   */
  toggleAgree() {
    this.agree = !this.agree;
  }

  /**
   * register user
   */
  registerUser() {
    this.formGroup.markAllAsTouched();

    if (this.formGroup.invalid) {
      return;
    }

    if (!this.agree) {
      this.messageService.open('error', 'Please agree to Terms of User and Privacy Policy');
      return;
    }

    this.inventory.unSubscribe('register');
    this.loading = true;

    const sub = this.userService
      .registerUser(this.email.value, this.password.value, this.captcha.value, this.question.value, this.answer.value)
      .subscribe({
        next: () => {
          this.messageService.open('default', 'User registered');
          this.router.navigate(['/main/login']);
        },
        error: (err) => {
          this.messageService.open('error', err.message);
          this.loading = false;
        }
      });

    this.inventory.store('register', sub);
  }
  redirectLogin() {
    this.toggleService.loginSub.next(true);
    this.router.navigate(['/main/login']);
  }
}
