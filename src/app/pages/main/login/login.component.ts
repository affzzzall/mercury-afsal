import { Component, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { MainBaseComponent } from '../common/main-base.component';
import { UserService } from '../../../services/user.service';
import { FormGroup } from '@angular/forms';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { MessageService } from '../../../components/common/message/services/message.service';
import { StorageService } from '../../../services/storage.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { appStoreActions } from '../../../stores/app-store/app-store.actions';
import { ToggleService } from 'src/app/services/common/toggle.service';

export interface LoginSubscriptions extends SubscriptionKeys {
  login: SubscriptionItem;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends MainBaseComponent implements OnInit, AfterViewInit, OnDestroy {
  // remember user state
  rememberUser = false;
  // form group for validation
  formGroup: FormGroup = new FormGroup({
    email: this.email,
    password: this.password
  });
  // loading state
  loading = false;
  // subscription inventory
  loginOpen: boolean;
  private inventory: SubscriptionInventory<LoginSubscriptions> = new SubscriptionInventory<LoginSubscriptions>();

  constructor(
    private store: Store,
    private router: Router,
    private userService: UserService,
    private messageService: MessageService,
    private storageService: StorageService,
    private toggleService: ToggleService
  ) {
    super();
  }

  ngOnInit(): void {
    this.toggleService.loginSub.subscribe((val) => {
      this.loginOpen = val;
    });
  }

  ngAfterViewInit() {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * toggle remember user state
   */
  toggleRememberUser() {
    this.rememberUser = !this.rememberUser;
  }

  /**
   * login
   */
  login() {
    // check validation
    this.formGroup.markAllAsTouched();

    if (this.formGroup.invalid) {
      return;
    }

    this.inventory.unSubscribe('login');
    this.loading = true;

    // call service
    const sub = this.userService.login(this.email.value, this.password.value).subscribe({
      next: (user) => {
        // set user to store
        this.store.dispatch(appStoreActions.setUser({ user }));

        // set local user if remember checked
        if (this.rememberUser) {
          this.storageService.localUser = user;
        }

        // go to home page
        this.router.navigate(['/dashboard/home']);
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('login', sub);
  }
  register() {
    this.router.navigate(['/main/register']);
  }
}
