import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

import { MainComponent } from './main.component';
import { ApplicationStateService } from 'src/app/services/common/application-state.service';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'login',
        loadChildren: () => import('./login/login.module').then((m) => m.LoginModule)
      },
      {
        path: 'register',
        loadChildren: () => import('./register/register.module').then((m) => m.RegisterModule)
      },
      {
        path: 'password-recovery',
        loadChildren: () => import('./password-recovery/password-recovery.module').then((m) => m.PasswordRecoveryModule)
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
  // public constructor(private router: Router,
  //                    private applicationStateService: ApplicationStateService) {
  //   if (applicationStateService.getIsMobileResolution()) {
  //     router.resetConfig(mobileRoutes);
  //   }
  // }
}
