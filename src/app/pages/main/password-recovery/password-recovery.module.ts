import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordRecoveryRoutingModule } from './password-recovery-routing.module';
import { PasswordRecoveryComponent } from './password-recovery.component';
import { EmailVerifyStepModule } from '../../../components/main/password-recovery/email-verify-step/email-verify-step.module';
import { QuestionVerifyStepModule } from '../../../components/main/password-recovery/question-verify-step/question-verify-step.module';
import { RecoveryEmailSentModule } from '../../../components/main/password-recovery/recovery-email-sent/recovery-email-sent.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [PasswordRecoveryComponent],
  imports: [
    CommonModule,
    PasswordRecoveryRoutingModule,
    EmailVerifyStepModule,
    QuestionVerifyStepModule,
    RecoveryEmailSentModule,
    RouterModule
  ]
})
export class PasswordRecoveryModule {}
