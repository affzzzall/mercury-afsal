import { Component, OnInit } from '@angular/core';
import { RegisterComponent } from './register/register.component';
import { StorageService } from '../../services/storage.service';
import { Router } from '@angular/router';
import { ToggleService } from 'src/app/services/common/toggle.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  // cookie visible state
  cookieVisible = !this.storageService.cookieClosed;
  // hide cookie message
  hideCookie = false;
  loginOpen = true;
  registerOpen = true;

  constructor(private storageService: StorageService, private router: Router, private toggleService: ToggleService) {}

  ngOnInit(): void {}

  onRouteActivated(component) {
    this.hideCookie = component instanceof RegisterComponent;
  }
  /**
   * closing cookie
   */
  onCookieCloseClicked() {
    this.cookieVisible = false;
    this.storageService.cookieClosed = true;
  }

  redirectLogin() {
    this.toggleService.loginSub.next(this.loginOpen);
  }

  redirectRegister() {
    this.router.navigate(['/main/register']);
  }
}
