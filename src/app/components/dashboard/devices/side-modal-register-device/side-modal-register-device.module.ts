import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalRegisterDeviceComponent } from './side-modal-register-device.component';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { DeviceRegisterDefaultComponent } from './device-register-default/device-register-default.component';
import { NoteModule } from '../../../common/note/note.module';
import { StepperModule } from '../../../common/stepper/stepper.module';
import { FormFieldModule } from '../../../common/fields/form-field/form-field.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { DeviceRegisterToolkitComponent } from './device-register-toolkit/device-register-toolkit.component';
import { ToolkitInfoModule } from '../../../common/toolkit-info/toolkit-info.module';
import { AuthCodeGeneratorModule } from '../../../common/auth-code-generator/auth-code-generator.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { DeviceRegisterSuccessComponent } from './device-register-success/device-register-success.component';
import { IconVerifiedModule } from '../../../common/icons/icon-verified/icon-verified.module';
import { OutlinedButtonModule } from '../../../common/buttons/outlined-button/outlined-button.module';

@NgModule({
  declarations: [
    SideModalRegisterDeviceComponent,
    DeviceRegisterDefaultComponent,
    DeviceRegisterToolkitComponent,
    DeviceRegisterSuccessComponent
  ],
  imports: [
    CommonModule,
    SideModalModule,
    CustomScrollbarModule,
    StepperModule,
    FormFieldModule,
    NoteModule,
    InlineButtonModule,
    FlatButtonModule,
    ToolkitInfoModule,
    AuthCodeGeneratorModule,
    SpinnerModule,
    IconVerifiedModule,
    OutlinedButtonModule
  ],
  exports: [SideModalRegisterDeviceComponent]
})
export class SideModalRegisterDeviceModule {}
