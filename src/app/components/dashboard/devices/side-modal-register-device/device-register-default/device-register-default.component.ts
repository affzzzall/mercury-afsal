import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

export interface DeviceRegisterData {
  name: string;
  description: string;
}

@Component({
  selector: 'app-device-register-default',
  templateUrl: './device-register-default.component.html',
  styleUrls: ['../common/register-device-base.component.scss', './device-register-default.component.scss']
})
export class DeviceRegisterDefaultComponent implements OnInit {
  // set data
  @Input() set data(data: DeviceRegisterData) {
    this.setFieldValues(data);
  }
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // next click
  @Output() nextClick: EventEmitter<DeviceRegisterData> = new EventEmitter<DeviceRegisterData>();
  // name
  name: FormControl = new FormControl('', Validators.required);
  // description
  description: FormControl = new FormControl('');
  // form group
  formGroup: FormGroup = new FormGroup({
    name: this.name,
    description: this.description
  });
  // name errors
  nameErrors = {
    required: 'The name is required'
  };

  constructor() {}

  ngOnInit(): void {}

  /**
   * go to next step when step is 2
   * @param step changed step
   */
  onStepChanged(step: number) {
    if (step === 2) {
      this.emitNextClick();
    }
  }

  /**
   * emit cancel click
   */
  emitCancelClick() {
    this.cancelClick.emit();
  }

  /**
   * emit next click
   */
  emitNextClick() {
    this.formGroup.markAllAsTouched();

    if (this.formGroup.invalid) {
      return;
    }

    this.nextClick.emit(this.formGroup.getRawValue());
  }

  /**
   * set values to field
   * @param data data
   */
  private setFieldValues(data: DeviceRegisterData) {
    if (data) {
      this.name.setValue(data.name || '');
      this.description.setValue(data.description || '');
    }
  }
}
