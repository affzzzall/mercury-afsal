import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceRegisterToolkitComponent } from './device-register-toolkit.component';

describe('DeviceRegisterToolkitComponent', () => {
  let component: DeviceRegisterToolkitComponent;
  let fixture: ComponentFixture<DeviceRegisterToolkitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceRegisterToolkitComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceRegisterToolkitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
