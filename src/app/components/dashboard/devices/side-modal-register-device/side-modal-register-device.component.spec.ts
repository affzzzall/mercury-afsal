import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalRegisterDeviceComponent } from './side-modal-register-device.component';

describe('SideModalRegisterDeviceComponent', () => {
  let component: SideModalRegisterDeviceComponent;
  let fixture: ComponentFixture<SideModalRegisterDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalRegisterDeviceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalRegisterDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
