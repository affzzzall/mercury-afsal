import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceInfoDefaultComponent } from './device-info-default.component';

describe('DeviceInfoDefaultComponent', () => {
  let component: DeviceInfoDefaultComponent;
  let fixture: ComponentFixture<DeviceInfoDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceInfoDefaultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceInfoDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
