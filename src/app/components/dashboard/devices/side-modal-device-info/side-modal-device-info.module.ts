import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalDeviceInfoComponent } from './side-modal-device-info.component';
import { DeviceInfoDefaultComponent } from './device-info-default/device-info-default.component';
import { DeviceInfoEditComponent } from './device-info-edit/device-info-edit.component';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { LabeledValueModule } from '../../../common/labeled-value/labeled-value.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { SideDeviceInfoModule } from '../../common/side-device-info/side-device-info.module';
import { FormFieldModule } from '../../../common/fields/form-field/form-field.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';

@NgModule({
  declarations: [SideModalDeviceInfoComponent, DeviceInfoDefaultComponent, DeviceInfoEditComponent],
  exports: [SideModalDeviceInfoComponent],
  imports: [
    CommonModule,
    FlatButtonModule,
    InlineButtonModule,
    LabeledValueModule,
    CustomScrollbarModule,
    SideModalModule,
    SideDeviceInfoModule,
    FormFieldModule,
    SpinnerModule
  ]
})
export class SideModalDeviceInfoModule {}
