import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalDeviceInfoComponent } from './side-modal-device-info.component';

describe('SideModalDeviceInfoComponent', () => {
  let component: SideModalDeviceInfoComponent;
  let fixture: ComponentFixture<SideModalDeviceInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalDeviceInfoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalDeviceInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
