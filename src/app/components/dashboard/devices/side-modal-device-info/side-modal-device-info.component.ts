import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { InfoBaseComponent } from '../../common/info/info-base.component';
import { AppDevice } from '../../../../models/data/app-device';

@Component({
  selector: 'app-side-modal-device-info',
  templateUrl: './side-modal-device-info.component.html',
  styleUrls: ['./side-modal-device-info.component.scss']
})
export class SideModalDeviceInfoComponent extends InfoBaseComponent implements OnInit {
  // device
  @Input() device: AppDevice;

  constructor(protected changeDetectorRef: ChangeDetectorRef) {
    super(changeDetectorRef);
  }

  ngOnInit(): void {}

  /**
   * update device data when device saved
   * @param device new device data
   */
  onDeviceSaved(device: AppDevice) {
    this.device.name = device.name;

    this.toDefault();
    this.updateInfoHeight();
    this.emitUpdated();
  }
}
