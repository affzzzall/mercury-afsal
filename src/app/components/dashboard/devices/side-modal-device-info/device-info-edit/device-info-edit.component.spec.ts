import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceInfoEditComponent } from './device-info-edit.component';

describe('DeviceInfoEditComponent', () => {
  let component: DeviceInfoEditComponent;
  let fixture: ComponentFixture<DeviceInfoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceInfoEditComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceInfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
