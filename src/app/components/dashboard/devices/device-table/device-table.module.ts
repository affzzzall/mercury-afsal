import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeviceTableComponent } from './device-table.component';
import { TableModule } from '../../../common/table/table.module';
import { AssociationsModule } from '../../../common/associations/associations.module';
import { UserInfoModule } from '../../../common/user-info/user-info.module';
import { TooltipModule } from '../../../common/tooltip/tooltip.module';
import { StateLabelModule } from '../../../common/state-label/state-label.module';
import { MoreOptionsModule } from '../../../common/more-options/more-options.module';
import { IconButtonModule } from '../../../common/buttons/icon-button/icon-button.module';
import { IconPencilModule } from '../../../common/icons/icon-pencil/icon-pencil.module';
import { IconMapModule } from '../../../common/icons/icon-map/icon-map.module';
import { SideModalAddAssociationModule } from '../../common/side-modal-add-association/side-modal-add-association.module';
import { SideModalAccessHistoryModule } from '../../common/side-modal-access-history/side-modal-access-history.module';
import { SideModalDeviceInfoModule } from '../side-modal-device-info/side-modal-device-info.module';

@NgModule({
  declarations: [DeviceTableComponent],
  exports: [DeviceTableComponent],
  imports: [
    CommonModule,
    TableModule,
    AssociationsModule,
    UserInfoModule,
    TooltipModule,
    StateLabelModule,
    MoreOptionsModule,
    IconButtonModule,
    IconPencilModule,
    IconMapModule,
    SideModalAddAssociationModule,
    SideModalAccessHistoryModule,
    SideModalDeviceInfoModule
  ]
})
export class DeviceTableModule {}
