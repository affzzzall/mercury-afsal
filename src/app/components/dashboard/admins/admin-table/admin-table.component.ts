import { ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { TableColumn, TableColumnSortDirection } from '../../../../models/common/table-column';
import { AppAdmin } from '../../../../models/data/app-user';
import { TableBaseComponent } from '../../common/table/table-base.component';
import { AdminService } from '../../../../services/admin.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { MessageService } from '../../../common/message/services/message.service';
import { SideModalAdminInfoComponent } from '../side-modal-admin-info/side-modal-admin-info.component';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { appStoreSelectors } from '../../../../stores/app-store/app-store.selectors';

export interface SortChanged {
  [key: string]: TableColumnSortDirection;
}

interface AdminTableSubscriptions extends SubscriptionKeys {
  deleteAdmin: SubscriptionItem;
}

@Component({
  selector: 'app-admin-table',
  templateUrl: './admin-table.component.html',
  styleUrls: ['../../common/table/table-base.component.scss', './admin-table.component.scss']
})
export class AdminTableComponent extends TableBaseComponent<AppAdmin> implements OnInit, OnDestroy {
  // admin deleted
  @Output() deleted: EventEmitter<void> = new EventEmitter<void>();
  // delete undo
  @Output() deleteUndo: EventEmitter<void> = new EventEmitter<void>();
  // admin info side modal
  @ViewChild(SideModalAdminInfoComponent) sideModalAdminInfo: SideModalAdminInfoComponent;
  // selected admin for admin info / access history / delete
  selectedAdmin: AppAdmin;
  // admin info visible state
  adminInfoVisible = false;
  // access history visible state
  accessHistoryVisible = false;
  // activity history visible state
  activityHistoryVisible = false;
  // delete modal visible state
  deleteModalVisible = false;
  // delete loading
  deleteLoading = false;
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.adminEditPermission);
  // access history read permission
  accessHistoryReadPermission$: Observable<boolean> = this.store.select(appStoreSelectors.accessHistoryReadPermission);
  // inventory
  private inventory: SubscriptionInventory<AdminTableSubscriptions> = new SubscriptionInventory<
    AdminTableSubscriptions
  >();

  // Load more label
  loadMoreLable = 'Admin';
  constructor(
    private store: Store,
    private adminService: AdminService,
    private messageService: MessageService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super();

    // set table columns
    this.columns = [
      new TableColumn('empty', ''),
      new TableColumn('id', 'ID', true),
      new TableColumn('name', 'NAME', true),
      new TableColumn('state', 'STATE'),
      new TableColumn('permissions', 'ACCESS PERMISSION'),
      new TableColumn('toolkit', 'TOOLKIT SN'),
      new TableColumn('recentActivity', 'RECENT ACTIVITY')
    ];
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * open admin info
   * @param user user
   */
  openAdminInfo(user: AppAdmin) {
    this.selectedAdmin = user;
    this.adminInfoVisible = true;
  }

  /**
   * close admin info
   */
  closeAdminInfo() {
    this.selectedAdmin = null;
    this.adminInfoVisible = false;
  }

  /**
   * open access history
   * @param user user
   */
  openAccessHistory(user: AppAdmin) {
    this.selectedAdmin = user;
    this.accessHistoryVisible = true;
  }

  /**
   * close access history
   */
  closeAccessHistory() {
    this.selectedAdmin = null;
    this.accessHistoryVisible = false;
  }

  /**
   * open delete confirm modal
   * @param user user to delete
   */
  openDeleteConfirmModal(user: AppAdmin) {
    this.selectedAdmin = user;
    this.deleteModalVisible = true;
  }

  /**
   * close delete confirm modal
   */
  closeDeleteConfirmModal() {
    this.selectedAdmin = null;
    this.deleteModalVisible = false;
  }

  /**
   * delete admin user and reload table
   */
  deleteAdmin() {
    this.inventory.unSubscribe('deleteAdmin');
    this.deleteLoading = true;

    const user = this.selectedAdmin;
    const sub = this.adminService.deleteAdmin(user).subscribe({
      next: () => {
        this.closeDeleteConfirmModal();
        this.deleted.emit();
        this.deleteLoading = false;
        this.messageService.open('default', `Administrator ${user.id} has been deleted`, true, () => {
          this.deleteUndo.emit();
        });
      },
      error: (err) => {
        this.closeDeleteConfirmModal();
        this.messageService.open('error', err.message);
        this.deleteLoading = false;
      }
    });

    this.inventory.store('deleteAdmin', sub);
  }

  /**
   * open activity history
   * @param user user
   */
  openActivityHistory(user: AppAdmin) {
    this.selectedAdmin = user;
    this.activityHistoryVisible = true;
  }

  /**
   * close activity history
   */
  closeActivityHistory() {
    this.selectedAdmin = null;
    this.activityHistoryVisible = false;
  }

  /**
   * open edit info
   * @param user user
   */
  openEditAdmin(user: AppAdmin) {
    this.openAdminInfo(user);
    this.changeDetectorRef.detectChanges();

    if (this.sideModalAdminInfo) {
      this.sideModalAdminInfo.toEdit();
    }
  }
}
