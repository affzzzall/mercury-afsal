import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalAdminInfoComponent } from './side-modal-admin-info.component';

describe('SideModalAdminInfoComponent', () => {
  let component: SideModalAdminInfoComponent;
  let fixture: ComponentFixture<SideModalAdminInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalAdminInfoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalAdminInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
