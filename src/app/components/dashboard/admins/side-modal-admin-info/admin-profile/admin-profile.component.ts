import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppAdmin } from '../../../../../models/data/app-user';
import { Observable } from 'rxjs';
import { appStoreSelectors } from '../../../../../stores/app-store/app-store.selectors';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['../common/admin-info-base.component.scss', './admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {
  // user
  @Input() user: AppAdmin;
  // edit click emitter
  @Output() editClick: EventEmitter<void> = new EventEmitter<void>();
  // close click emitter
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.adminEditPermission);

  constructor(private store: Store) {}

  ngOnInit(): void {}

  /**
   * emit edit click
   */
  emitEditClick() {
    this.editClick.emit();
  }

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }
}
