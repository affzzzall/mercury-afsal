import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { AppAdmin } from '../../../../models/data/app-user';
import { InfoBaseComponent } from '../../common/info/info-base.component';

@Component({
  selector: 'app-side-modal-admin-info',
  templateUrl: './side-modal-admin-info.component.html',
  styleUrls: ['./side-modal-admin-info.component.scss']
})
export class SideModalAdminInfoComponent extends InfoBaseComponent implements OnInit {
  // user
  @Input() user: AppAdmin;

  constructor(protected changeDetectorRef: ChangeDetectorRef) {
    super(changeDetectorRef);
  }

  ngOnInit(): void {}

  /**
   * update user data when user saved
   * @param user new user data
   */
  onUserSaved(user: AppAdmin) {
    this.user.firstName = user.firstName;
    this.user.lastName = user.lastName;
    this.user.state = user.state;
    this.user.disabledReason = user.disabledReason;
    this.user.status = user.status;
    this.user.permissions = user.permissions;

    this.toDefault();
    this.updateInfoHeight();
    this.emitUpdated();
  }
}
