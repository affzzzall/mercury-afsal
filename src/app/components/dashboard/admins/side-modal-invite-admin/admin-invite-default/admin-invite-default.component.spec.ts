import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInviteDefaultComponent } from './admin-invite-default.component';

describe('AdminInviteDefaultComponent', () => {
  let component: AdminInviteDefaultComponent;
  let fixture: ComponentFixture<AdminInviteDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminInviteDefaultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInviteDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
