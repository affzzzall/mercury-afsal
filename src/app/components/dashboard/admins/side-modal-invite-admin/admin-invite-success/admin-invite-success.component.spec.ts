import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInviteSuccessComponent } from './admin-invite-success.component';

describe('AdminInviteSuccessComponent', () => {
  let component: AdminInviteSuccessComponent;
  let fixture: ComponentFixture<AdminInviteSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminInviteSuccessComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInviteSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
