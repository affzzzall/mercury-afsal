import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInvitePermissionsComponent } from './admin-invite-permissions.component';

describe('AdminInvitePermissionsComponent', () => {
  let component: AdminInvitePermissionsComponent;
  let fixture: ComponentFixture<AdminInvitePermissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminInvitePermissionsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInvitePermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
