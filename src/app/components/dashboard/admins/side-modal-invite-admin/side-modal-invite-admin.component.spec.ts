import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalInviteAdminComponent } from './side-modal-invite-admin.component';

describe('SideModalInviteAdminComponent', () => {
  let component: SideModalInviteAdminComponent;
  let fixture: ComponentFixture<SideModalInviteAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalInviteAdminComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalInviteAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
