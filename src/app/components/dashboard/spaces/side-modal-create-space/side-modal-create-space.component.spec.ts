import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalCreateSpaceComponent } from './side-modal-create-space.component';

describe('SideModalCreateSpaceComponent', () => {
  let component: SideModalCreateSpaceComponent;
  let fixture: ComponentFixture<SideModalCreateSpaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalCreateSpaceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalCreateSpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
