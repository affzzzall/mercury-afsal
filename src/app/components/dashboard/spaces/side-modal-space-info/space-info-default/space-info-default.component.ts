import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppSpace } from '../../../../../models/data/app-space';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { appStoreSelectors } from '../../../../../stores/app-store/app-store.selectors';

@Component({
  selector: 'app-space-info-default',
  templateUrl: './space-info-default.component.html',
  styleUrls: ['../../../common/info/info-base.component.scss', './space-info-default.component.scss']
})
export class SpaceInfoDefaultComponent implements OnInit {
  // space
  @Input() space: AppSpace;
  // edit click
  @Output() editClick: EventEmitter<void> = new EventEmitter<void>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.spaceEditPermission);

  constructor(private store: Store) {}

  ngOnInit(): void {}
}
