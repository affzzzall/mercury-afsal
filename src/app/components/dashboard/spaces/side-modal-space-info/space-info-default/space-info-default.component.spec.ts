import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaceInfoDefaultComponent } from './space-info-default.component';

describe('SpaceInfoDefaultComponent', () => {
  let component: SpaceInfoDefaultComponent;
  let fixture: ComponentFixture<SpaceInfoDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpaceInfoDefaultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceInfoDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
