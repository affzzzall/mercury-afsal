import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusSpaceComponent } from './status-space.component';
import { StatusItemModule } from '../status-item/status-item.module';
import { StatusFooterModule } from '../status-footer/status-footer.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [StatusSpaceComponent],
  imports: [CommonModule, StatusItemModule, StatusFooterModule, RouterModule]
})
export class StatusSpaceModule {}
