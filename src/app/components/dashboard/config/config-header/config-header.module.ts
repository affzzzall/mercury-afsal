import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigHeaderComponent } from './config-header.component';
import { IconConfigModule } from '../../../common/icons/icon-config/icon-config.module';
import { OutlinedButtonModule } from '../../../common/buttons/outlined-button/outlined-button.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';

@NgModule({
  declarations: [ConfigHeaderComponent],
  exports: [ConfigHeaderComponent],
  imports: [CommonModule, IconConfigModule, OutlinedButtonModule, FlatButtonModule]
})
export class ConfigHeaderModule {}
