import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppStatus } from '../../../../models/data/app-status';

@Component({
  selector: 'app-status-item',
  templateUrl: './status-item.component.html',
  styleUrls: ['./status-item.component.scss']
})
export class StatusItemComponent implements OnInit {
  // status
  @Input() status: AppStatus;
  // read only state
  @Input() readOnly = false;
  // edit click
  @Output() editClick: EventEmitter<AppStatus> = new EventEmitter<AppStatus>();
  // delete click
  @Output() deleteClick: EventEmitter<AppStatus> = new EventEmitter<AppStatus>();

  constructor() {}

  ngOnInit(): void {}
}
