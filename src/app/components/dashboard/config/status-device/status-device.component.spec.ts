import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusDeviceComponent } from './status-device.component';

describe('StatusDeviceComponent', () => {
  let component: StatusDeviceComponent;
  let fixture: ComponentFixture<StatusDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StatusDeviceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
