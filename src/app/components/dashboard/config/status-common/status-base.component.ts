import { AppStatus, AppStatusGroup } from '../../../../models/data/app-status';
import { ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { appStoreSelectors } from '../../../../stores/app-store/app-store.selectors';
import { map } from 'rxjs/operators';

export class StatusBaseComponent {
  // edit status
  @Output() editStatus: EventEmitter<AppStatus> = new EventEmitter<AppStatus>();
  // delete status
  @Output() deleteStatus: EventEmitter<AppStatus> = new EventEmitter<AppStatus>();
  // add status clicked
  @Output() addStatusClick: EventEmitter<AppStatusGroup> = new EventEmitter<AppStatusGroup>();
  // statuses
  // statuses should be set from the out of the router
  statuses: AppStatus[] = [];
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.configEditPermission);

  constructor(protected store: Store, protected changeDetectorRef: ChangeDetectorRef) {}

  get readOnly$() {
    return this.editPermission$.pipe(
      map((permission) => {
        return !permission;
      })
    );
  }

  /**
   * set statuses
   * @param statuses statuses
   */
  setStatuses(statuses: AppStatus[]) {
    this.statuses = statuses;
    this.changeDetectorRef.detectChanges();
  }

  /**
   * emit edit status
   * @param status status
   */
  emitEditStatus(status: AppStatus) {
    this.editStatus.emit(status);
  }

  /**
   * emit delete status
   * @param status status
   */
  emitDeleteStatus(status: AppStatus) {
    this.deleteStatus.emit(status);
  }

  /**
   * emit add status
   * @param group group
   */
  emitAddStatus(group: AppStatusGroup) {
    this.addStatusClick.emit(group);
  }
}
