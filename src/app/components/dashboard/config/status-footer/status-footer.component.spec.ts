import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusFooterComponent } from './status-footer.component';

describe('StatusFooterComponent', () => {
  let component: StatusFooterComponent;
  let fixture: ComponentFixture<StatusFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StatusFooterComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
