import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalStatusEditComponent } from './side-modal-status-edit.component';

describe('SideModalStatusEditComponent', () => {
  let component: SideModalStatusEditComponent;
  let fixture: ComponentFixture<SideModalStatusEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalStatusEditComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalStatusEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
