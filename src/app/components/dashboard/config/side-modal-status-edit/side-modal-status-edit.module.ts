import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalStatusEditComponent } from './side-modal-status-edit.component';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { FormFieldModule } from '../../../common/fields/form-field/form-field.module';
import { CheckboxModule } from '../../../common/checkbox/checkbox.module';
import { RadioModule } from '../../../common/radio/radio.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';

@NgModule({
  declarations: [SideModalStatusEditComponent],
  exports: [SideModalStatusEditComponent],
  imports: [
    CommonModule,
    SideModalModule,
    FlatButtonModule,
    InlineButtonModule,
    CustomScrollbarModule,
    FormFieldModule,
    CheckboxModule,
    RadioModule,
    SpinnerModule
  ]
})
export class SideModalStatusEditModule {}
