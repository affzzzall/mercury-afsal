import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessHistoryTableComponent } from './access-history-table.component';

describe('AccessHistoryTableComponent', () => {
  let component: AccessHistoryTableComponent;
  let fixture: ComponentFixture<AccessHistoryTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccessHistoryTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessHistoryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
