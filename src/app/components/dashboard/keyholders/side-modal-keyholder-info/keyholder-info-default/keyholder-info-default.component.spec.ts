import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyholderInfoDefaultComponent } from './keyholder-info-default.component';

describe('KeyholderInfoDefaultComponent', () => {
  let component: KeyholderInfoDefaultComponent;
  let fixture: ComponentFixture<KeyholderInfoDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KeyholderInfoDefaultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyholderInfoDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
