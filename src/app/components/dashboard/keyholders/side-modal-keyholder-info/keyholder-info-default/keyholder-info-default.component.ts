import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppKeyholder } from '../../../../../models/data/app-user';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../../../../../stores/app-store/app-store.selectors';

@Component({
  selector: 'app-keyholder-info-default',
  templateUrl: './keyholder-info-default.component.html',
  styleUrls: ['../../../common/info/info-base.component.scss', './keyholder-info-default.component.scss']
})
export class KeyholderInfoDefaultComponent implements OnInit {
  // keyholder
  @Input() user: AppKeyholder;
  // edit click
  @Output() editClick: EventEmitter<void> = new EventEmitter<void>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // edit permission stream
  editPermission$: Observable<boolean> = this.store.select(appStoreSelectors.keyholderEditPermission);

  constructor(private store: Store) {}

  ngOnInit(): void {}
}
