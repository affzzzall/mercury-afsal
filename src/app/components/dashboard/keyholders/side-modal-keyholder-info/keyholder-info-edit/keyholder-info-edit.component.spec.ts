import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyholderInfoEditComponent } from './keyholder-info-edit.component';

describe('KeyholderInfoEditComponent', () => {
  let component: KeyholderInfoEditComponent;
  let fixture: ComponentFixture<KeyholderInfoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KeyholderInfoEditComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyholderInfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
