import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalKeyholderInfoComponent } from './side-modal-keyholder-info.component';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { SideUserInfoModule } from '../../common/side-user-info/side-user-info.module';
import { KeyholderInfoDefaultComponent } from './keyholder-info-default/keyholder-info-default.component';
import { KeyholderInfoEditComponent } from './keyholder-info-edit/keyholder-info-edit.component';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { LabeledValueModule } from '../../../common/labeled-value/labeled-value.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { FormFieldModule } from '../../../common/fields/form-field/form-field.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { CheckboxModule } from '../../../common/checkbox/checkbox.module';

@NgModule({
  declarations: [SideModalKeyholderInfoComponent, KeyholderInfoDefaultComponent, KeyholderInfoEditComponent],
  exports: [SideModalKeyholderInfoComponent],
  imports: [
    CommonModule,
    SideModalModule,
    SideUserInfoModule,
    CustomScrollbarModule,
    LabeledValueModule,
    FlatButtonModule,
    InlineButtonModule,
    FormFieldModule,
    SpinnerModule,
    CheckboxModule
  ]
})
export class SideModalKeyholderInfoModule {}
