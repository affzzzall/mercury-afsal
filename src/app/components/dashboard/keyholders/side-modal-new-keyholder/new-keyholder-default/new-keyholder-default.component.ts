import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { KeyholderService } from '../../../../../services/keyholder.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../../utils/subscribe.util';
import { MessageService } from '../../../../common/message/services/message.service';
import { OptionItem } from '../../../../../models/common/option-item';
import { Store } from '@ngrx/store';
import { appStoreSelectors } from '../../../../../stores/app-store/app-store.selectors';

export interface NewKeyholderData {
  name: string;
  email: string;
  status: string;
  description: string;
  allowPinReleaseShackle: boolean;
  pin: string;
  payload: string;
}

interface NewKeyholderSubscriptions extends SubscriptionKeys {
  newKeyholder: SubscriptionItem;
}

@Component({
  selector: 'app-new-keyholder-default',
  templateUrl: './new-keyholder-default.component.html',
  styleUrls: ['../common/new-keyholder-base.component.scss', './new-keyholder-default.component.scss']
})
export class NewKeyholderDefaultComponent implements OnInit, OnDestroy {
  // set data
  @Input() set data(data: NewKeyholderData) {
    this.setFieldValues(data);
  }
  // submitted
  @Output() submitted: EventEmitter<void> = new EventEmitter<void>();
  // cancel click
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();
  // name
  name: FormControl = new FormControl('', Validators.required);
  // email
  email: FormControl = new FormControl('', [Validators.required, Validators.email]);
  // description
  description: FormControl = new FormControl('');
  // pin
  pin: FormControl = new FormControl('', Validators.pattern(/^\d{4}$/));
  // payload
  payload: FormControl = new FormControl('', Validators.pattern(/^[0-9A-Fa-f]*$/));
  // status
  status: FormControl = new FormControl('', Validators.required);
  // status options
  statusOptions: OptionItem[] = [];
  // allow pin release
  allowPinRelease = false;
  // name errors
  nameErrors = {
    required: 'The name is required'
  };
  // email errors
  emailErrors = {
    required: 'The email is required',
    email: 'The email is invalid'
  };
  // status errors
  statusErrors = {
    required: 'The status is required'
  };
  // pin errors
  pinErrors = {
    pattern: 'The pin should be 4 digits number'
  };
  // payload errors
  payloadErrors = {
    pattern: 'Invalid payload'
  };
  // loading
  loading = false;
  // form group
  private formGroup: FormGroup = new FormGroup({
    name: this.name,
    email: this.email,
    status: this.status,
    description: this.description,
    pin: this.pin,
    payload: this.payload
  });
  // inventory
  private inventory: SubscriptionInventory<NewKeyholderSubscriptions> = new SubscriptionInventory<
    NewKeyholderSubscriptions
  >();

  constructor(
    private store: Store,
    private keyholderService: KeyholderService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    const sub = this.store.select(appStoreSelectors.statuses).subscribe((statuses) => {
      this.statusOptions = statuses
        .filter((status) => status.group === 'keyholder')
        .map((status) => new OptionItem(status.label, status.label));
    });

    this.inventory.store('statuses$', sub);
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * set field values
   * @param data data
   */
  private setFieldValues(data: NewKeyholderData) {
    if (data) {
      this.name.setValue(data.name);
    }
  }

  /**
   * submit data
   */
  submit() {
    this.formGroup.markAllAsTouched();

    if (this.formGroup.invalid) {
      return;
    }

    this.inventory.unSubscribe('newKeyholder');
    this.loading = true;

    const sub = this.keyholderService
      .createNewKeyholder({
        name: this.name.value,
        email: this.email.value,
        status: this.status.value,
        description: this.description.value,
        allowPinReleaseShackle: this.allowPinRelease,
        pin: this.pin.value,
        payload: this.payload.value
      })
      .subscribe({
        next: () => {
          this.submitted.emit();
        },
        error: (err) => {
          this.messageService.open('error', err.message);
          this.loading = false;
        }
      });

    this.inventory.store('newKeyholder', sub);
  }
}
