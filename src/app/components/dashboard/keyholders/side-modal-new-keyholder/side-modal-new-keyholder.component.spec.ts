import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalNewKeyholderComponent } from './side-modal-new-keyholder.component';

describe('SideModalNewKeyholderComponent', () => {
  let component: SideModalNewKeyholderComponent;
  let fixture: ComponentFixture<SideModalNewKeyholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalNewKeyholderComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalNewKeyholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
