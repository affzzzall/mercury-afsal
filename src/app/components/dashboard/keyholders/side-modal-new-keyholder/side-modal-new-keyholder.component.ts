import { Component, EventEmitter, OnInit, Output } from '@angular/core';

export type SideModalNewKeyholderView = 'default' | 'success';

@Component({
  selector: 'app-side-modal-new-keyholder',
  templateUrl: './side-modal-new-keyholder.component.html',
  styleUrls: ['./side-modal-new-keyholder.component.scss']
})
export class SideModalNewKeyholderComponent implements OnInit {
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // created
  @Output() created: EventEmitter<void> = new EventEmitter<void>();
  // view type
  view: SideModalNewKeyholderView = 'default';

  constructor() {}

  ngOnInit(): void {}

  /**
   * on submitted
   */
  onSubmitted() {
    this.created.emit();
    this.view = 'success';
  }
}
