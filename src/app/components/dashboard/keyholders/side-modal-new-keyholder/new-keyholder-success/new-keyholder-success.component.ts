import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-new-keyholder-success',
  templateUrl: './new-keyholder-success.component.html',
  styleUrls: ['../common/new-keyholder-base.component.scss', './new-keyholder-success.component.scss']
})
export class NewKeyholderSuccessComponent implements OnInit {
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // add new click
  @Output() addNewClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}
}
