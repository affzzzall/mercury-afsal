import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewKeyholderSuccessComponent } from './new-keyholder-success.component';

describe('NewKeyholderSuccessComponent', () => {
  let component: NewKeyholderSuccessComponent;
  let fixture: ComponentFixture<NewKeyholderSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewKeyholderSuccessComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewKeyholderSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
