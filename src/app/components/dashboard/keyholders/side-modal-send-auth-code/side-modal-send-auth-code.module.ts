import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalSendAuthCodeComponent } from './side-modal-send-auth-code.component';
import { SideModalModule } from '../../common/side-modal/side-modal.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { AuthCodeGeneratorModule } from '../../../common/auth-code-generator/auth-code-generator.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';

@NgModule({
  declarations: [SideModalSendAuthCodeComponent],
  exports: [SideModalSendAuthCodeComponent],
  imports: [CommonModule, SideModalModule, FlatButtonModule, AuthCodeGeneratorModule, CustomScrollbarModule]
})
export class SideModalSendAuthCodeModule {}
