import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalSendAuthCodeComponent } from './side-modal-send-auth-code.component';

describe('SideModalSendAuthCodeComponent', () => {
  let component: SideModalSendAuthCodeComponent;
  let fixture: ComponentFixture<SideModalSendAuthCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalSendAuthCodeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalSendAuthCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
