import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyholderTableComponent } from './keyholder-table.component';

describe('KeyholderTableComponent', () => {
  let component: KeyholderTableComponent;
  let fixture: ComponentFixture<KeyholderTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KeyholderTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyholderTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
