import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeyholderTableComponent } from './keyholder-table.component';
import { TableModule } from '../../../common/table/table.module';
import { UserInfoModule } from '../../../common/user-info/user-info.module';
import { StateLabelModule } from '../../../common/state-label/state-label.module';
import { AssociationsModule } from '../../../common/associations/associations.module';
import { MoreOptionsModule } from '../../../common/more-options/more-options.module';
import { IconButtonModule } from '../../../common/buttons/icon-button/icon-button.module';
import { IconLockSmallModule } from '../../../common/icons/icon-lock-small/icon-lock-small.module';
import { IconLineChartModule } from '../../../common/icons/icon-line-chart/icon-line-chart.module';
import { IconPencilModule } from '../../../common/icons/icon-pencil/icon-pencil.module';
import { IconDeleteModule } from '../../../common/icons/icon-delete/icon-delete.module';
import { IconShieldModule } from '../../../common/icons/icon-shield/icon-shield.module';
import { SideModalAccessHistoryModule } from '../../common/side-modal-access-history/side-modal-access-history.module';
import { SideModalAddAssociationModule } from '../../common/side-modal-add-association/side-modal-add-association.module';
import { SideModalActivityHistoryModule } from '../../common/side-modal-activity-history/side-modal-activity-history.module';
import { DeleteModalModule } from '../../../common/delete-modal/delete-modal.module';
import { SideModalSendAuthCodeModule } from '../side-modal-send-auth-code/side-modal-send-auth-code.module';
import { SideModalKeyholderInfoModule } from '../side-modal-keyholder-info/side-modal-keyholder-info.module';

@NgModule({
  declarations: [KeyholderTableComponent],
  exports: [KeyholderTableComponent],
  imports: [
    CommonModule,
    TableModule,
    UserInfoModule,
    StateLabelModule,
    AssociationsModule,
    MoreOptionsModule,
    IconButtonModule,
    IconLockSmallModule,
    IconLineChartModule,
    IconPencilModule,
    IconDeleteModule,
    IconShieldModule,
    SideModalAccessHistoryModule,
    SideModalAddAssociationModule,
    SideModalActivityHistoryModule,
    DeleteModalModule,
    SideModalSendAuthCodeModule,
    SideModalKeyholderInfoModule
  ]
})
export class KeyholderTableModule {}
