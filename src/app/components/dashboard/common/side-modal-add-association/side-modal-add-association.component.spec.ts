import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalAddAssociationComponent } from './side-modal-add-association.component';

describe('SideModalAddAssociationComponent', () => {
  let component: SideModalAddAssociationComponent;
  let fixture: ComponentFixture<SideModalAddAssociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalAddAssociationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalAddAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
