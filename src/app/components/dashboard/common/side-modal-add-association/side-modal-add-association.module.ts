import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalAddAssociationComponent } from './side-modal-add-association.component';
import { SideModalModule } from '../side-modal/side-modal.module';
import { SideUserInfoModule } from '../side-user-info/side-user-info.module';
import { AssociatedItemsModule } from '../associated-items/associated-items.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { UnassociatedItemsModule } from '../unassociated-items/unassociated-items.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../../../common/buttons/inline-button/inline-button.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { SideDeviceInfoModule } from '../side-device-info/side-device-info.module';
import { SideSpaceInfoModule } from '../side-space-info/side-space-info.module';

@NgModule({
  declarations: [SideModalAddAssociationComponent],
  exports: [SideModalAddAssociationComponent],
  imports: [
    CommonModule,
    SideModalModule,
    SideUserInfoModule,
    AssociatedItemsModule,
    CustomScrollbarModule,
    UnassociatedItemsModule,
    FlatButtonModule,
    InlineButtonModule,
    SpinnerModule,
    SideDeviceInfoModule,
    SideSpaceInfoModule
  ]
})
export class SideModalAddAssociationModule {}
