import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { DeviceService } from '../../../../services/device.service';
import { AppKeyholder } from '../../../../models/data/app-user';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { MessageService } from '../../../common/message/services/message.service';
import { FormControl } from '@angular/forms';
import { AppAssociationType } from '../../../../models/data/app-associations';
import { SpaceService } from '../../../../services/space.service';
import { KeyholderService } from '../../../../services/keyholder.service';

export type AppUnassociatedItemType = 'device' | 'space' | 'keyholder';
export type AppUnassociatedItemSort = 'createdAt' | 'name';

interface UnassociatedItemsSubscriptions extends SubscriptionKeys {
  getItems: SubscriptionItem;
}

@Component({
  selector: 'app-unassociated-items',
  templateUrl: './unassociated-items.component.html',
  styleUrls: ['./unassociated-items.component.scss']
})
export class UnassociatedItemsComponent implements OnInit, OnDestroy {
  // type
  @Input() type: AppUnassociatedItemType;
  // associations
  @Input() set associations(associations: AppAssociationType[]) {
    this.createExistingAssociationMap(associations);
  }
  // add click
  @Output() addClick: EventEmitter<AppAssociationType> = new EventEmitter<AppAssociationType>();
  // sort by
  sortBy: AppUnassociatedItemSort = 'createdAt';
  // items
  items: AppAssociationType[] = [];
  // search
  search: FormControl = new FormControl('');
  // loading
  loading = false;
  // existing associations
  existingAssociationMap: { [key: string]: boolean } = {};
  // inventory
  private inventory: SubscriptionInventory<UnassociatedItemsSubscriptions> = new SubscriptionInventory<
    UnassociatedItemsSubscriptions
  >();

  constructor(
    private deviceService: DeviceService,
    private spaceService: SpaceService,
    private keyholderService: KeyholderService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getItems();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * return true when sort by new
   */
  get sortByNew() {
    return this.sortBy === 'createdAt';
  }

  /**
   * return true when sort by name
   */
  get sortByName() {
    return this.sortBy === 'name';
  }

  /**
   * change sort
   * @param sortBy sort by
   */
  changeSort(sortBy: AppUnassociatedItemSort) {
    this.sortBy = sortBy;
    this.getItems();
  }

  /**
   * create existing associations map
   * @param associations associations
   */
  private createExistingAssociationMap(associations: AppAssociationType[]) {
    if (associations) {
      this.existingAssociationMap = {};

      associations.forEach((association) => {
        this.existingAssociationMap[association.id] = true;
      });
    }
  }

  /**
   * get items
   */
  getItems() {
    switch (this.type) {
      case 'device': {
        this.getDevices();
        break;
      }

      case 'space': {
        this.getSpaces();
        break;
      }

      case 'keyholder': {
        this.getKeyholders();
        break;
      }
    }
  }

  /**
   * get devices
   */
  private getDevices() {
    this.inventory.unSubscribe('getItems');
    this.loading = true;

    const sub = this.deviceService.getUnassociatedDevices(this.sortBy).subscribe({
      next: (devices) => {
        this.items = devices;
        this.loading = false;
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('getItems', sub);
  }

  /**
   * get spaces
   */
  private getSpaces() {
    this.inventory.unSubscribe('getItems');
    this.loading = true;

    const sub = this.spaceService.getUnassociatedSpaces(this.sortBy).subscribe({
      next: (spaces) => {
        this.items = spaces;
        this.loading = false;
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('getItems', sub);
  }

  /**
   * get keyholders
   */
  private getKeyholders() {
    this.inventory.unSubscribe('getItems');
    this.loading = true;

    const sub = this.keyholderService.getUnassociatedKeyholders(this.sortBy).subscribe({
      next: (keyholders) => {
        this.items = keyholders;
        this.loading = false;
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('getItems', sub);
  }

  /**
   * emit add click
   * @param item item to add
   */
  emitAddClick(item: AppAssociationType) {
    this.addClick.emit(item);
  }

  /**
   * filtered items without search
   */
  get filteredItemWithoutSearch() {
    return this.items.filter((item) => !this.existingAssociationMap[item.id]);
  }

  /**
   * filtered items
   */
  get filteredItems() {
    return this.filteredItemWithoutSearch.filter((item) => {
      return (
        item.name.search(new RegExp(this.search.value, 'i')) !== -1 ||
        item.id.search(new RegExp(this.search.value, 'i')) !== -1
      );
    });
  }
}
