import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalAccessHistoryComponent } from './side-modal-access-history.component';

describe('SideModalAccessHistoryComponent', () => {
  let component: SideModalAccessHistoryComponent;
  let fixture: ComponentFixture<SideModalAccessHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalAccessHistoryComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalAccessHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
