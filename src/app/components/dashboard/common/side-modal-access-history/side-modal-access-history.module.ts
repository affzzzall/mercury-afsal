import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalAccessHistoryComponent } from './side-modal-access-history.component';
import { SideModalModule } from '../side-modal/side-modal.module';
import { SideUserInfoModule } from '../side-user-info/side-user-info.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { FlatButtonModule } from '../../../common/buttons/flat-button/flat-button.module';
import { IconMapModule } from '../../../common/icons/icon-map/icon-map.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';
import { SideDeviceInfoModule } from '../side-device-info/side-device-info.module';
import { SideSpaceInfoModule } from '../side-space-info/side-space-info.module';

@NgModule({
  declarations: [SideModalAccessHistoryComponent],
  exports: [SideModalAccessHistoryComponent],
  imports: [
    CommonModule,
    SideModalModule,
    SideUserInfoModule,
    CustomScrollbarModule,
    FlatButtonModule,
    IconMapModule,
    SpinnerModule,
    SideDeviceInfoModule,
    SideSpaceInfoModule
  ]
})
export class SideModalAccessHistoryModule {}
