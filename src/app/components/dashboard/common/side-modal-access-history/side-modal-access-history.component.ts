import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import { AppDevice } from '../../../../models/data/app-device';
import { AccessHistoryService } from '../../../../services/access-history.service';
import { AppAccessHistory } from '../../../../models/data/app-access-history';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { MessageService } from '../../../common/message/services/message.service';
import { AppUser } from '../../../../models/data/app-user';
import { Observable } from 'rxjs';
import { PageResponse } from '../../../../models/common/page-response';
import { CustomScrollbarComponent } from '../../../common/custom-scrollbar/custom-scrollbar.component';
import { getElement } from '../../../../utils/element.util';
import { AppSpace } from '../../../../models/data/app-space';

interface AccessHistorySubscriptions extends SubscriptionKeys {
  getHistories: SubscriptionItem;
}

export type AppAccessHistoryType = 'keyholder' | 'device' | 'space';

@Component({
  selector: 'app-side-modal-access-history',
  templateUrl: './side-modal-access-history.component.html',
  styleUrls: ['./side-modal-access-history.component.scss']
})
export class SideModalAccessHistoryComponent implements OnInit, OnDestroy {
  // user
  @Input() user: AppUser;
  // device
  @Input() device: AppDevice;
  // space
  @Input() space: AppSpace;
  // access history type
  @Input() type: AppAccessHistoryType;
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // custom scrollbar element
  @ViewChild(CustomScrollbarComponent, { read: ElementRef }) customScrollbar: ElementRef<HTMLElement>;
  // page
  page = 0;
  // size;
  size = 10;
  // total
  total = 0;
  // histories
  histories: AppAccessHistory[] = [];
  // loading
  loading = false;
  // inventory
  private inventory: SubscriptionInventory<AccessHistorySubscriptions> = new SubscriptionInventory<
    AccessHistorySubscriptions
  >();

  constructor(
    private renderer: Renderer2,
    private accessHistoryService: AccessHistoryService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getHistories();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * set scroll content height
   * @param info info
   */
  onInfoInit(info: HTMLElement) {
    setTimeout(() => {
      const content = getElement(this.customScrollbar);

      if (content && info) {
        this.renderer.setStyle(content, 'height', `calc(100% - 60px - 78px - ${info.offsetHeight}px)`);
      }
    });
  }

  /**
   * get histories
   * @param page page
   * @param size size
   */
  getHistories(page = this.page, size = this.size) {
    let of: Observable<PageResponse<AppAccessHistory>>;

    // switch by type
    switch (this.type) {
      case 'device': {
        of = this.accessHistoryService.getAccessHistoryByDevice(this.device, page, size);
        break;
      }

      case 'keyholder': {
        of = this.accessHistoryService.getAccessHistoryByUser(this.user, page, size);
        break;
      }

      case 'space': {
        of = this.accessHistoryService.getAccessHistoryAppSpace(this.space, page, size);
        break;
      }
    }

    if (of) {
      if (page === 0) {
        this.histories = [];
      }

      this.inventory.unSubscribe('getHistories');
      this.loading = true;

      const sub = of.subscribe({
        next: (res) => {
          this.histories.push(...res.data);
          this.page = res.page;
          this.size = res.size;
          this.total = res.total;
          this.loading = false;
        },
        error: (err) => {
          this.messageService.open('error', err.message);
          this.loading = false;
        }
      });

      this.inventory.store('getHistories', sub);
    }
  }

  /**
   * return true when there is more page
   */
  get hasMore() {
    return this.total > this.histories.length;
  }

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }
}
