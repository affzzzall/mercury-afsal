import { ChangeDetectorRef, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { SideUserInfoComponent } from '../side-user-info/side-user-info.component';
import { getElement } from '../../../../utils/element.util';

export type InfoView = 'default' | 'edit';

export class InfoBaseComponent {
  // updated
  @Output() updated: EventEmitter<void> = new EventEmitter<void>();
  // close click
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // side user info
  @ViewChild(SideUserInfoComponent, { read: ElementRef }) sideUserInfo: ElementRef<HTMLElement>;
  // info height
  infoHeight = 184;
  // info view
  view: InfoView = 'default';

  constructor(protected changeDetectorRef: ChangeDetectorRef) {}

  /**
   * to default view
   */
  toDefault() {
    this.view = 'default';
  }

  /**
   * to edit view
   */
  toEdit() {
    this.view = 'edit';
  }

  /**
   * get content height
   */
  get contentHeight() {
    return `calc(100% - ${this.infoHeight}px)`;
  }

  /**
   * on info init
   * @param info info element
   */
  onInfoInit(info: HTMLElement) {
    if (info) {
      this.updateInfoHeight();
    }
  }

  /**
   * update info height
   */
  updateInfoHeight() {
    const info = getElement(this.sideUserInfo);

    if (info) {
      this.infoHeight = info.offsetHeight;
      this.changeDetectorRef.detectChanges();
    }
  }

  /**
   * emit close click
   */
  emitCloseClick() {
    this.closeClick.emit();
  }

  /**
   * updated emit
   */
  emitUpdated() {
    this.updated.emit();
  }
}
