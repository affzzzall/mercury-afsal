import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationSelectorComponent } from './location-selector.component';
import { IconSearchModule } from '../../../common/icons/icon-search/icon-search.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { ReactiveFormsModule } from '@angular/forms';
import { OutsideDetectorModule } from '../../../common/outside-detector/outside-detector.module';
import { IconDropdownModule } from '../../../common/icons/icon-dropdown/icon-dropdown.module';

@NgModule({
  declarations: [LocationSelectorComponent],
  exports: [LocationSelectorComponent],
  imports: [
    CommonModule,
    IconSearchModule,
    CustomScrollbarModule,
    ReactiveFormsModule,
    OutsideDetectorModule,
    IconDropdownModule
  ]
})
export class LocationSelectorModule {}
