import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideUserInfoComponent } from './side-user-info.component';
import { UserAvatarModule } from '../../../common/user-avatar/user-avatar.module';
import { IconCameraModule } from '../../../common/icons/icon-camera/icon-camera.module';
import { IconToolkitModule } from '../../../common/icons/icon-toolkit/icon-toolkit.module';

@NgModule({
  declarations: [SideUserInfoComponent],
  exports: [SideUserInfoComponent],
  imports: [CommonModule, UserAvatarModule, IconCameraModule, IconToolkitModule]
})
export class SideUserInfoModule {}
