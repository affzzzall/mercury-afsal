import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardHeaderComponent } from './dashboard-header.component';
import { LogoModule } from '../../../common/logo/logo.module';
import { RouterModule } from '@angular/router';
import { LocationSelectorModule } from '../location-selector/location-selector.module';
import { IconHelpModule } from '../../../common/icons/icon-help/icon-help.module';
import { TooltipModule } from '../../../common/tooltip/tooltip.module';
import { UserMenuModule } from '../user-menu/user-menu.module';
import { IconPencilModule } from 'src/app/components/common/icons/icon-pencil/icon-pencil.module';
import { IconExitModule } from 'src/app/components/common/icons/icon-exit/icon-exit.module';

@NgModule({
  declarations: [DashboardHeaderComponent],
  exports: [DashboardHeaderComponent],
  imports: [
    CommonModule,
    LogoModule,
    RouterModule,
    LocationSelectorModule,
    IconHelpModule,
    TooltipModule,
    UserMenuModule,
    IconPencilModule,
    IconExitModule
  ]
})
export class DashboardHeaderModule {}
