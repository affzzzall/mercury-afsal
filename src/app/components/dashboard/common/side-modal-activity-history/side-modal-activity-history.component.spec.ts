import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalActivityHistoryComponent } from './side-modal-activity-history.component';

describe('SideModalActivityHistoryComponent', () => {
  let component: SideModalActivityHistoryComponent;
  let fixture: ComponentFixture<SideModalActivityHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalActivityHistoryComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalActivityHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
