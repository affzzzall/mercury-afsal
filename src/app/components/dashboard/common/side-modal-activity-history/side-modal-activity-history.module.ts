import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalActivityHistoryComponent } from './side-modal-activity-history.component';
import { SideModalModule } from '../side-modal/side-modal.module';
import { SideUserInfoModule } from '../side-user-info/side-user-info.module';
import { CustomScrollbarModule } from '../../../common/custom-scrollbar/custom-scrollbar.module';
import { SpinnerModule } from '../../../common/spinner/spinner.module';

@NgModule({
  declarations: [SideModalActivityHistoryComponent],
  exports: [SideModalActivityHistoryComponent],
  imports: [CommonModule, SideModalModule, SideUserInfoModule, CustomScrollbarModule, SpinnerModule]
})
export class SideModalActivityHistoryModule {}
