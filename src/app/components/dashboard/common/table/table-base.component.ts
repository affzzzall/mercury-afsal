import { EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { SortChanged } from '../../admins/admin-table/admin-table.component';
import { TableColumn } from '../../../../models/common/table-column';
import { PagingChangeEvent } from '../../../common/paginator/models/paging-change-event';
import { cloneDeep } from '../../../../utils/lodash.util';

export class TableBaseComponent<T> {
  // page
  @Input() page = 0;
  // size
  @Input() size = 10;
  // total
  @Input() total = 0;
  // data
  @Input() data: T[] = [];
  // loading state
  @Input() loading = false;
  // sorts change emitter
  @Output() sortsChange: EventEmitter<SortChanged> = new EventEmitter<SortChanged>();
  // paging change event
  @Output() pagingChange: EventEmitter<PagingChangeEvent> = new EventEmitter<PagingChangeEvent>();
  // default class for table base
  @HostBinding('class.app-dashboard-table') class = true;
  // columns
  columns: TableColumn[] = [];

  /**
   * emit sorts change
   */
  emitSortsChange() {
    const sorts = {};

    this.columns.forEach((column) => {
      if (column.useSort) {
        sorts[column.property] = column.direction;
      }
    });

    this.sortsChange.emit(sorts);
  }

  /**
   * emit paging change
   * @param event event
   */
  emitPagingChange(event: PagingChangeEvent) {
    this.pagingChange.emit(event);
  }

  /**
   * reset data when info updated
   */
  onInfoUpdated() {
    this.data = cloneDeep(this.data);
  }
}
