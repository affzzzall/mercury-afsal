import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalTitleComponent } from './side-modal-title.component';

describe('SideModalTitleComponent', () => {
  let component: SideModalTitleComponent;
  let fixture: ComponentFixture<SideModalTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalTitleComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
