import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalHeaderComponent } from './side-modal-header.component';

describe('SideModalHeaderComponent', () => {
  let component: SideModalHeaderComponent;
  let fixture: ComponentFixture<SideModalHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalHeaderComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
