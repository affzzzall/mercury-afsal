import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideModalComponent } from './side-modal.component';
import { IconCloseModule } from '../../../common/icons/icon-close/icon-close.module';
import { SideModalTitleComponent } from './side-modal-title/side-modal-title.component';
import { SideModalContentComponent } from './side-modal-content/side-modal-content.component';
import { SideModalFooterComponent } from './side-modal-footer/side-modal-footer.component';
import { SideModalHeaderComponent } from './side-modal-header/side-modal-header.component';

@NgModule({
  declarations: [
    SideModalComponent,
    SideModalTitleComponent,
    SideModalContentComponent,
    SideModalFooterComponent,
    SideModalHeaderComponent
  ],
  exports: [
    SideModalComponent,
    SideModalContentComponent,
    SideModalTitleComponent,
    SideModalFooterComponent,
    SideModalHeaderComponent
  ],
  imports: [CommonModule, IconCloseModule]
})
export class SideModalModule {}
