import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalFooterComponent } from './side-modal-footer.component';

describe('SideModalFooterComponent', () => {
  let component: SideModalFooterComponent;
  let fixture: ComponentFixture<SideModalFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalFooterComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
