import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserMenuComponent } from './user-menu.component';
import { UserInfoModule } from '../../../common/user-info/user-info.module';
import { IconDropdownModule } from '../../../common/icons/icon-dropdown/icon-dropdown.module';
import { CardModule } from '../../../common/card/card.module';
import { IconExitModule } from '../../../common/icons/icon-exit/icon-exit.module';
import { IconPencilModule } from '../../../common/icons/icon-pencil/icon-pencil.module';
import { IconButtonModule } from '../../../common/buttons/icon-button/icon-button.module';
import { OutsideDetectorModule } from '../../../common/outside-detector/outside-detector.module';
import { SideModalEditUserModule } from '../side-modal-edit-user/side-modal-edit-user.module';

@NgModule({
  declarations: [UserMenuComponent],
  exports: [UserMenuComponent],
  imports: [
    CommonModule,
    UserInfoModule,
    IconDropdownModule,
    CardModule,
    IconExitModule,
    IconPencilModule,
    IconButtonModule,
    OutsideDetectorModule,
    SideModalEditUserModule
  ]
})
export class UserMenuModule {}
