import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { StorageService } from '../../../../services/storage.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppUser } from '../../../../models/data/app-user';
import { appStoreSelectors } from '../../../../stores/app-store/app-store.selectors';
import { appStoreActions } from '../../../../stores/app-store/app-store.actions';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ToggleService } from 'src/app/services/common/toggle.service';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
  // menu opened state
  opened = false;
  // edit user visible state
  editUserVisible = false;
  // user stream
  user$: Observable<AppUser> = this.store.select(appStoreSelectors.user);

  constructor(
    public elementRef: ElementRef<HTMLElement>,
    private store: Store,
    private router: Router,
    private storageService: StorageService,
    private navToogled: ToggleService
  ) {}

  ngOnInit(): void {
    this.openEditUserMobile();
  }

  /**
   * open menu
   */
  open() {
    this.opened = true;
  }

  /**
   * close menu
   */
  close() {
    this.opened = false;
  }

  /**
   * toggle opened state
   */
  toggle() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }

  /**
   * process user logout
   */
  logout() {
    // go to main page
    this.router.navigate(['/main/login']).then(() => {
      // clear user
      this.store.dispatch(appStoreActions.setUser({ user: null }));
      // clear storage user
      this.storageService.localUser = null;
    });
  }

  /**
   * open edit user
   */
  openEditUser() {
    this.close();
    this.editUserVisible = true;
  }

  /**
   * close edit user
   */
  closeEditUser() {
    this.editUserVisible = false;
  }

  openEditUserMobile() {
    this.navToogled.toggleNav.subscribe((val) => {
      this.editUserVisible = val;
    });
  }
}
