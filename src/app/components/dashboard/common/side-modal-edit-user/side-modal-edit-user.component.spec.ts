import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideModalEditUserComponent } from './side-modal-edit-user.component';

describe('SideModalEditUserComponent', () => {
  let component: SideModalEditUserComponent;
  let fixture: ComponentFixture<SideModalEditUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideModalEditUserComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideModalEditUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
