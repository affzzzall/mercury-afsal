import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppAdmin } from '../../../../../models/data/app-user';

@Component({
  selector: 'app-edit-user-profile',
  templateUrl: './edit-user-profile.component.html',
  styleUrls: ['../common/edit-user-base.component.scss', './edit-user-profile.component.scss']
})
export class EditUserProfileComponent implements OnInit {
  // user
  @Input() user: AppAdmin;
  // change email click emitter
  @Output() changeEmailClick: EventEmitter<void> = new EventEmitter<void>();
  // toolkit configuration click emitter
  @Output() toolkitConfigurationClick: EventEmitter<void> = new EventEmitter<void>();
  // done click emitter
  @Output() doneClick: EventEmitter<void> = new EventEmitter<void>();
  // cancel click emitter
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit change email click
   */
  emitChangeEmailClick() {
    this.changeEmailClick.emit();
  }

  /**
   * emit toolkit configuration click
   */
  emitToolkitConfigurationClick() {
    this.toolkitConfigurationClick.emit();
  }

  /**
   * emit done click
   */
  emitDoneClick() {
    this.doneClick.emit();
  }

  /**
   * emit cancel click
   */
  emitCancelClick() {
    this.cancelClick.emit();
  }
}
