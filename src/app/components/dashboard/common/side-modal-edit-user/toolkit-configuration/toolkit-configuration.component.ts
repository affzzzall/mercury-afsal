import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppAdmin } from '../../../../../models/data/app-user';

@Component({
  selector: 'app-toolkit-configuration',
  templateUrl: './toolkit-configuration.component.html',
  styleUrls: ['../common/edit-user-base.component.scss', './toolkit-configuration.component.scss']
})
export class ToolkitConfigurationComponent implements OnInit {
  // user
  @Input() user: AppAdmin;
  // done click emitter
  @Output() doneClick: EventEmitter<void> = new EventEmitter<void>();
  // cancel click emitter
  @Output() cancelClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit cancel click
   */
  emitCancelClick() {
    this.cancelClick.emit();
  }
}
