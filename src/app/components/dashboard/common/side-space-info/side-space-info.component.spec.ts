import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideSpaceInfoComponent } from './side-space-info.component';

describe('SideSpaceInfoComponent', () => {
  let component: SideSpaceInfoComponent;
  let fixture: ComponentFixture<SideSpaceInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SideSpaceInfoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideSpaceInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
