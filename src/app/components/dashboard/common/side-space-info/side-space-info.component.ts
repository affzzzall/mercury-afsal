import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { SideInfoBaseComponent } from '../side-info-common/side-info-base.component';
import { AppSpace } from '../../../../models/data/app-space';

@Component({
  selector: 'app-side-space-info',
  templateUrl: './side-space-info.component.html',
  styleUrls: ['../side-info-common/side-info-base.component.scss', './side-space-info.component.scss']
})
export class SideSpaceInfoComponent extends SideInfoBaseComponent implements OnInit {
  // space
  @Input() space: AppSpace;

  constructor(protected elementRef: ElementRef<HTMLElement>) {
    super(elementRef);
  }

  ngOnInit(): void {}
}
