export class AppPage {
  page: number;
  label: string;
  active: boolean;

  constructor(page: number = 0, active: boolean = false) {
    this.page = page;
    this.active = active;
    this.label = `${page + 1}`;
  }
}
