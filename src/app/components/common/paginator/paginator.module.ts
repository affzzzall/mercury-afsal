import { NgModule } from '@angular/core';
import { EllipsisPaginatorComponent } from './ellipsis-paginator/ellipsis-paginator.component';
import { CommonModule } from '@angular/common';
import { IconArrowLeftModule } from '../icons/icon-arrow-left/icon-arrow-left.module';
import { IconArrowRightModule } from '../icons/icon-arrow-right/icon-arrow-right.module';
import { IconArrowDropDownModule } from '../icons/icon-arrow-drop-down/icon-arrow-drop-down.module';
import { IconArrowDropUpModule } from '../icons/icon-arrow-drop-up/icon-arrow-drop-up.module';
import { OutsideDetectorModule } from '../outside-detector/outside-detector.module';
import { CustomScrollbarModule } from '../custom-scrollbar/custom-scrollbar.module';
import { IconChevronLeftModule } from '../icons/icon-chevron-left/icon-chevron-left.module';
import { IconChevronRightModule } from '../icons/icon-chevron-right/icon-chevron-right.module';

@NgModule({
  declarations: [EllipsisPaginatorComponent],
  imports: [
    CommonModule,
    IconArrowLeftModule,
    IconArrowRightModule,
    IconArrowDropDownModule,
    IconArrowDropUpModule,
    OutsideDetectorModule,
    CustomScrollbarModule,
    IconChevronLeftModule,
    IconChevronRightModule
  ],
  exports: [EllipsisPaginatorComponent]
})
export class PaginatorModule {}
