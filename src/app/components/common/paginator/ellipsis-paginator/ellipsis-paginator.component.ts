import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { PaginatorCommonComponent } from '../common/paginator-common.component';
import { AppPage } from '../models/app-page';
import { OptionItem } from '../../../../models/common/option-item';
import { PagingChangeEvent } from '../models/paging-change-event';

@Component({
  selector: 'app-ellipsis-paginator',
  templateUrl: './ellipsis-paginator.component.html',
  styleUrls: ['./ellipsis-paginator.component.scss']
})
export class EllipsisPaginatorComponent extends PaginatorCommonComponent implements OnInit, OnChanges {
  @Input() display = 3;

  showStartEllipsis = false;
  showEndEllipsis = false;

  startPages: AppPage[] = [];
  middlePages: AppPage[] = [];
  endPages: AppPage[] = [];

  opened = false;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    this.setPagination();
  }

  /**
   * toggle
   */
  toggle() {
    this.opened = !this.opened;
  }

  /**
   * emit paging change
   * @param item item
   */
  onSizeChange(item: OptionItem<number>) {
    if (item.value !== this.size) {
      this.pagingChange.emit(new PagingChangeEvent(0, item.value));
    }
  }

  private setPagination() {
    this.defineExistenceOfPrevNext();
    this.createPages();
  }

  /**
   * create pages
   */
  private createPages() {
    const displayAll = this.totalPage <= this.display * 2;

    // hide start/end ellipsis before creating pages
    this.showStartEllipsis = false;
    this.showEndEllipsis = false;

    if (displayAll) {
      // if total size is too small, display all pages
      this.createStartPages(0, this.totalPage);
      this.createMiddlePages(0, 0);
      this.createEndPages(0, 0);
    } else {
      // half of display number
      const half = Math.floor(this.display / 2);
      // maximum end number of starting area
      const endOfStartingArea = this.page + 1 + half;
      // minimum start number of ending area
      const startOfEndingArea = this.page - half;

      // if page is in the start area, set as `true`
      const start = this.page < this.display + half;
      // if page is in the end area, set as `true`
      const end = this.page + 1 > this.totalPage - this.display - half;

      if (start) {
        // end point of start pages
        const startEnd = Math.max(endOfStartingArea, this.display);
        // start point of end pages
        const endStart = Math.max(startEnd, this.totalPage - this.display);

        this.createStartPages(0, startEnd);
        this.createMiddlePages(0, 0);
        this.createEndPages(endStart, this.totalPage);
        // only show ellipsis if `startEnd` and `endStart` is different
        this.showStartEllipsis = startEnd !== endStart;
      } else if (end) {
        this.createStartPages(0, this.display);
        this.createMiddlePages(0, 0);
        this.createEndPages(Math.min(startOfEndingArea, this.totalPage - this.display), this.totalPage);
        // show end ellipsis
        this.showEndEllipsis = true;
      } else {
        this.createStartPages(0, half);
        this.createMiddlePages(startOfEndingArea, endOfStartingArea);
        this.createEndPages(this.totalPage - half, this.totalPage);
        this.showStartEllipsis = true;
        this.showEndEllipsis = true;
      }
    }
  }

  /**
   * create start pages
   * @param start start
   * @param end end
   */
  private createStartPages(start: number, end: number) {
    this.startPages = [];

    for (let i = start; i < end; i++) {
      this.startPages.push(new AppPage(i, this.page === i));
    }
  }

  /**
   * create middle pages
   * @param start start
   * @param end end
   */
  private createMiddlePages(start: number, end: number) {
    this.middlePages = [];

    for (let i = start; i < end; i++) {
      this.middlePages.push(new AppPage(i, this.page === i));
    }
  }

  /**
   * create end pages
   * @param start start
   * @param end end
   */
  private createEndPages(start: number, end: number) {
    this.endPages = [];

    for (let i = start; i < end; i++) {
      this.endPages.push(new AppPage(i, this.page === i));
    }
  }
}
