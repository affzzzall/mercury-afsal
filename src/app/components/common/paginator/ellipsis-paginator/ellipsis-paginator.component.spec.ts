import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EllipsisPaginatorComponent } from './ellipsis-paginator.component';

describe('EllipsisPaginatorComponent', () => {
  let component: EllipsisPaginatorComponent;
  let fixture: ComponentFixture<EllipsisPaginatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EllipsisPaginatorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EllipsisPaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
