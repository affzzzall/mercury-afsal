import { Component, ElementRef, OnInit } from '@angular/core';
import { AppVerticalPosition } from '../position-detector/position-detector.directive';

@Component({
  selector: 'app-more-options',
  templateUrl: './more-options.component.html',
  styleUrls: ['./more-options.component.scss']
})
export class MoreOptionsComponent implements OnInit {
  // opened state
  opened = false;
  // vertical align
  vertical: AppVerticalPosition = 'bottom';

  constructor(public elementRef: ElementRef<HTMLElement>) {}

  ngOnInit(): void {}

  /**
   * open
   */
  open() {
    this.opened = true;
  }

  /**
   * close
   */
  close() {
    this.opened = false;
  }

  /**
   * toggle opened
   */
  toggle() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }
}
