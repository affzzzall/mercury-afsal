import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PasswordFieldComponent } from './password-field.component';
import { FieldErrorModule } from '../field-error/field-error.module';
import { IconKeyModule } from '../../icons/icon-key/icon-key.module';
import { ReactiveFormsModule } from '@angular/forms';
import { IconEyeModule } from '../../icons/icon-eye/icon-eye.module';

@NgModule({
  declarations: [PasswordFieldComponent],
  exports: [PasswordFieldComponent],
  imports: [CommonModule, FieldErrorModule, IconKeyModule, ReactiveFormsModule, IconEyeModule]
})
export class PasswordFieldModule {}
