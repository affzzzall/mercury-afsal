import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldErrorComponent } from './field-error.component';
import { IconTriangleWarningModule } from '../../icons/icon-triangle-warning/icon-triangle-warning.module';

@NgModule({
  declarations: [FieldErrorComponent],
  exports: [FieldErrorComponent],
  imports: [CommonModule, IconTriangleWarningModule]
})
export class FieldErrorModule {}
