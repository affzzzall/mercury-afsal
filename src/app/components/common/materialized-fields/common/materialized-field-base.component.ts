import { FormControl } from '@angular/forms';
import { AfterViewInit, ElementRef, HostBinding, HostListener, Input, OnDestroy, ViewChild } from '@angular/core';
import { getElement } from '../../../../utils/element.util';

export class MaterializedFieldBaseComponent implements AfterViewInit, OnDestroy {
  // label
  @Input() label: string;
  // form control
  @Input() control: FormControl;
  // errors
  // key is error code, value is message
  @Input() errors: { [key: string]: string } = {};
  // input reference
  @ViewChild('input') inputRef: ElementRef<HTMLInputElement>;
  // set tabindex
  @HostBinding('attr.tabindex') tabindex = 0;
  // default class
  @HostBinding('class.app-form-field') class = true;
  // read disabled status as readonly
  @HostBinding('class.app-readonly') get readOnly() {
    return this.control.disabled;
  }

  // focused state
  focused = false;

  constructor() {
    this.detectAutoFill = this.detectAutoFill.bind(this);
  }

  ngAfterViewInit(): void {
    const input = getElement(this.inputRef);

    input.addEventListener('animationstart', this.detectAutoFill);
  }

  ngOnDestroy(): void {
    const input = getElement(this.inputRef);

    input.removeEventListener('animationstart', this.detectAutoFill);
  }

  /**
   * detect auto fill
   * @param animationName animation name
   */
  private detectAutoFill({ animationName }: AnimationEvent) {
    if (animationName === 'autoFilled') {
      this.focused = true;
    }
  }

  /**
   * return true when control isn't pristine || touched and has error code
   * @param code error code
   */
  hasError(code: string) {
    return (!this.control.pristine || this.control.touched) && this.control.hasError(code);
  }

  /**
   * return error code list
   */
  get errorCodes() {
    return Object.keys(this.errors);
  }

  /**
   * return true when control isn't pristine || touched and invalid
   */
  get invalid() {
    return (!this.control.pristine || this.control.touched) && this.control.invalid;
  }

  @HostListener('focus')
  onHostFocused() {
    this.focus();
  }

  @HostListener('click')
  onHostClicked() {
    this.focus();
  }

  /**
   * focus to the host
   */
  focus() {
    this.focused = true;

    const input = getElement(this.inputRef);

    if (input) {
      input.focus();
    }
  }

  /**
   * blur from the host
   */
  blur() {
    this.focused = false;

    const input = getElement(this.inputRef);

    if (input) {
      input.blur();
    }
  }
}
