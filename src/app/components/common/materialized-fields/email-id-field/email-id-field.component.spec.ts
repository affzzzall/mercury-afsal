import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailIdFieldComponent } from './email-id-field.component';

describe('EmailIdFieldComponent', () => {
  let component: EmailIdFieldComponent;
  let fixture: ComponentFixture<EmailIdFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailIdFieldComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailIdFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
