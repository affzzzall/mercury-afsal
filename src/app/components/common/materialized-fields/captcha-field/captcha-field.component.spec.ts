import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaptchaFieldComponent } from './captcha-field.component';

describe('CaptchaFieldComponent', () => {
  let component: CaptchaFieldComponent;
  let fixture: ComponentFixture<CaptchaFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CaptchaFieldComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaptchaFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
