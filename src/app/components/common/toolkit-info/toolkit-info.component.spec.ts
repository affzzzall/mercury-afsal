import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolkitInfoComponent } from './toolkit-info.component';

describe('ToolkitInfoComponent', () => {
  let component: ToolkitInfoComponent;
  let fixture: ComponentFixture<ToolkitInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToolkitInfoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolkitInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
