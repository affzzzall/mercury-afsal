import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolkitInfoComponent } from './toolkit-info.component';

@NgModule({
  declarations: [ToolkitInfoComponent],
  exports: [ToolkitInfoComponent],
  imports: [CommonModule]
})
export class ToolkitInfoModule {}
