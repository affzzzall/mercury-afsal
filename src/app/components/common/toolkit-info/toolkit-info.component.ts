import { Component, Input, OnInit } from '@angular/core';
import { AppToolkit } from '../../../models/data/app-toolkit';

@Component({
  selector: 'app-toolkit-info',
  templateUrl: './toolkit-info.component.html',
  styleUrls: ['./toolkit-info.component.scss']
})
export class ToolkitInfoComponent implements OnInit {
  // toolkit
  @Input() toolkit: AppToolkit;
  // dns visible state
  @Input() dnsVisible = true;

  constructor() {}

  ngOnInit(): void {}
}
