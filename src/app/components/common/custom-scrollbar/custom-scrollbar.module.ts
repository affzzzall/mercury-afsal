import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomScrollbarComponent } from './custom-scrollbar.component';
import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
  declarations: [CustomScrollbarComponent],
  exports: [CustomScrollbarComponent],
  imports: [CommonModule, PerfectScrollbarModule],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: {
        suppressScrollX: true
      }
    }
  ]
})
export class CustomScrollbarModule {}
