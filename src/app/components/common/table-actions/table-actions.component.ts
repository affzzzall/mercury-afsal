import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChildren
} from '@angular/core';
import { AppDynamicFieldConfig, DynamicFieldComponent } from '../dynamic-field/dynamic-field.component';

export type AppTableActionsViewType = 'map' | 'grid' | 'list';
export interface AppTableActionsViewTypes {
  map: boolean;
  grid: boolean;
  list: boolean;
}

@Component({
  selector: 'app-table-actions',
  templateUrl: './table-actions.component.html',
  styleUrls: ['./table-actions.component.scss']
})
export class TableActionsComponent implements OnInit {
  // table view types
  @Input() viewTypes: AppTableActionsViewTypes = {
    map: false,
    grid: false,
    list: false
  };
  // filters
  @Input() filters: { [key: string]: any } = {};
  // use export state
  @Input() useExports = true;
  // use filter state
  @Input() useFilters = true;
  // filter configs
  @Input() filterConfigs: AppDynamicFieldConfig[] = [];
  // view type
  @Input() viewType: AppTableActionsViewType = 'list';
  // view type change emitter
  @Output() viewTypeChange: EventEmitter<AppTableActionsViewType> = new EventEmitter<AppTableActionsViewType>();
  // filter apply emitter
  @Output() filterApply: EventEmitter<{ [key: string]: any }> = new EventEmitter<{ [p: string]: any }>();
  // dynamic fields
  @ViewChildren(DynamicFieldComponent) dynamicFields: QueryList<DynamicFieldComponent>;
  // start date
  startDate: Date;
  // end date
  endDate: Date;

  // filter opened state
  opened = false;

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngOnInit(): void {}

  /**
   * at least one value is true in viewTypes,
   * return true
   */
  get hasViewTypes() {
    return Object.keys(this.viewTypes).filter((key) => this.viewTypes[key]).length > 0;
  }

  /**
   * emit view type change
   * @param type type
   */
  emitViewTypeChange(type: AppTableActionsViewType) {
    this.viewTypeChange.emit(type);
  }

  /**
   * return true when view type is selected
   * @param type type
   */
  isSelected(type: AppTableActionsViewType) {
    return this.viewType === type;
  }

  /**
   * open
   */
  open() {
    this.opened = true;
    this.changeDetectorRef.detectChanges();
    this.setFilterValues();
  }

  /**
   * close
   */
  close() {
    this.opened = false;
  }

  /**
   * toggle opened
   */
  toggle() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }

  /**
   * create filter object
   */
  onApplyClicked() {
    const filter = {};

    this.dynamicFields.forEach((field) => {
      filter[field.config.label] = field.control.value;
    });

    this.filterApply.emit(filter);
  }

  /**
   * set filter values to fields
   */
  setFilterValues() {
    if (this.dynamicFields) {
      this.dynamicFields.forEach((field) => {
        const value = this.filters[field.config.label];

        field.control.setValue(value || '');
      });
    }
  }

  /**
   * clear filters
   */
  clearFilters() {
    this.dynamicFields.forEach((field) => field.control.setValue(''));
  }

  setStartDate(date: Date) {
    this.startDate = date;
  }

  setEndDate(date: Date) {
    this.endDate = date;
  }
}
