import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableActionsComponent } from './table-actions.component';
import { IconMapModule } from '../icons/icon-map/icon-map.module';
import { IconGridModule } from '../icons/icon-grid/icon-grid.module';
import { IconListModule } from '../icons/icon-list/icon-list.module';
import { OutlinedButtonModule } from '../buttons/outlined-button/outlined-button.module';
import { IconDocumentModule } from '../icons/icon-document/icon-document.module';
import { IconFilterModule } from '../icons/icon-filter/icon-filter.module';
import { DynamicFieldModule } from '../dynamic-field/dynamic-field.module';
import { FlatButtonModule } from '../buttons/flat-button/flat-button.module';
import { InlineButtonModule } from '../buttons/inline-button/inline-button.module';

@NgModule({
  declarations: [TableActionsComponent],
  exports: [TableActionsComponent],
  imports: [
    CommonModule,
    IconMapModule,
    IconGridModule,
    IconListModule,
    OutlinedButtonModule,
    IconDocumentModule,
    IconFilterModule,
    DynamicFieldModule,
    FlatButtonModule,
    InlineButtonModule
  ]
})
export class TableActionsModule {}
