import { Directive, HostBinding } from '@angular/core';
import { ButtonBaseComponent } from '../common/button-base.component';

@Directive({
  selector: '[appInlineButton]'
})
export class InlineButtonDirective extends ButtonBaseComponent {
  // inline button class
  @HostBinding('class.app-inline-button') inlineButtonClass = true;
}
