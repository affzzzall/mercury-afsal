import { Directive, HostBinding } from '@angular/core';
import { ButtonBaseComponent } from '../common/button-base.component';

@Directive({
  selector: '[appFlatButton]'
})
export class FlatButtonDirective extends ButtonBaseComponent {
  // add flat button class
  @HostBinding('class.app-flat-button') flatButtonClass = true;
}
