import { HostBinding, Input } from '@angular/core';

export type AppButtonTheme = 'primary' | 'warn' | 'secondary';

export class ButtonBaseComponent {
  // button theme
  @Input() @HostBinding('attr.app-theme') theme: AppButtonTheme = 'primary';
  // common class name
  @HostBinding('class.app-button') class = true;
}
