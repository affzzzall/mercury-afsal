import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconButtonDirective } from './icon-button.directive';

@NgModule({
  declarations: [IconButtonDirective],
  imports: [CommonModule],
  exports: [IconButtonDirective]
})
export class IconButtonModule {}
