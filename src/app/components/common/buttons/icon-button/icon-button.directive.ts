import { Directive, HostBinding } from '@angular/core';
import { ButtonBaseComponent } from '../common/button-base.component';

@Directive({
  selector: '[appIconButton]'
})
export class IconButtonDirective extends ButtonBaseComponent {
  // icon button class
  @HostBinding('class.app-icon-button') iconButtonClass = true;
}
