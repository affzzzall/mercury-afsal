import { Component, Input, OnInit } from '@angular/core';
import {
  ADMIN_ACTIVE_STATE,
  ADMIN_ACTIVE_COLOR,
  ADMIN_DISABLED_STATE,
  ADMIN_DISABLED_COLOR,
  ADMIN_INACTIVE_STATE,
  ADMIN_INACTIVE_COLOR,
  ADMIN_INVITED_STATE,
  ADMIN_INVITED_COLOR,
  DEVICE_ACTIVE_STATE,
  DEVICE_ACTIVE_COLOR,
  DEVICE_INACTIVE_STATE,
  DEVICE_INACTIVE_COLOR,
  DEVICE_UNREGISTERED_STATE,
  DEVICE_UNREGISTERED_COLOR,
  DEVICE_DISABLED_STATE,
  DEVICE_DISABLED_COLOR,
  DEVICE_LOW_BATTERY_STATE,
  DEVICE_LOW_BATTERY_COLOR,
  SPACE_DISABLED_STATE,
  SPACE_ACTIVE_COLOR,
  SPACE_DISABLED_COLOR,
  SPACE_ACTIVE_STATE,
  SPACE_INACTIVE_COLOR,
  SPACE_INACTIVE_STATE,
  KEYHOLDER_DISABLED_COLOR,
  KEYHOLDER_ACTIVE_COLOR,
  KEYHOLDER_DISABLED_STATE,
  KEYHOLDER_ACTIVE_STATE,
  KEYHOLDER_INACTIVE_STATE,
  KEYHOLDER_INACTIVE_COLOR,
  DEVICE_TAMPERED_COLOR,
  DEVICE_TAMPERED_STATE,
  DEVICE_ERROR_STATE,
  DEVICE_ERROR_COLOR
} from '../../../utils/constants.util';

export type AppStateType = 'admin' | 'keyholder' | 'device' | 'space';

@Component({
  selector: 'app-state-label',
  templateUrl: './state-label.component.html',
  styleUrls: ['./state-label.component.scss']
})
export class StateLabelComponent implements OnInit {
  // state
  @Input() state: string;
  // status
  @Input() status: string;
  // state type
  @Input() type: AppStateType;
  // disabled reason
  @Input() disabledReason: string;

  // color map
  colorMap = {
    admin: {
      [ADMIN_ACTIVE_STATE]: ADMIN_ACTIVE_COLOR,
      [ADMIN_INVITED_STATE]: ADMIN_INVITED_COLOR,
      [ADMIN_INACTIVE_STATE]: ADMIN_INACTIVE_COLOR,
      [ADMIN_DISABLED_STATE]: ADMIN_DISABLED_COLOR
    },
    keyholder: {
      [KEYHOLDER_ACTIVE_STATE]: KEYHOLDER_ACTIVE_COLOR,
      [KEYHOLDER_INACTIVE_STATE]: KEYHOLDER_INACTIVE_COLOR,
      [KEYHOLDER_DISABLED_STATE]: KEYHOLDER_DISABLED_COLOR
    },
    device: {
      [DEVICE_ACTIVE_STATE]: DEVICE_ACTIVE_COLOR,
      [DEVICE_INACTIVE_STATE]: DEVICE_INACTIVE_COLOR,
      [DEVICE_DISABLED_STATE]: DEVICE_DISABLED_COLOR,
      [DEVICE_LOW_BATTERY_STATE]: DEVICE_LOW_BATTERY_COLOR,
      [DEVICE_UNREGISTERED_STATE]: DEVICE_UNREGISTERED_COLOR,
      [DEVICE_TAMPERED_STATE]: DEVICE_TAMPERED_COLOR,
      [DEVICE_ERROR_STATE]: DEVICE_ERROR_COLOR
    },
    space: {
      [SPACE_ACTIVE_STATE]: SPACE_ACTIVE_COLOR,
      [SPACE_INACTIVE_STATE]: SPACE_INACTIVE_COLOR,
      [SPACE_DISABLED_STATE]: SPACE_DISABLED_COLOR
    }
  };

  constructor() {}

  ngOnInit(): void {}

  /**
   * get state color
   */
  get color() {
    return this.colorMap[this.type][this.state];
  }
}
