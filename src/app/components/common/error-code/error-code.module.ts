import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorCodeComponent } from './error-code.component';
import { CardModule } from '../card/card.module';
import { PositionDetectorModule } from '../position-detector/position-detector.module';
import { OutsideDetectorModule } from '../outside-detector/outside-detector.module';

@NgModule({
  declarations: [ErrorCodeComponent],
  exports: [ErrorCodeComponent],
  imports: [CommonModule, CardModule, PositionDetectorModule, OutsideDetectorModule]
})
export class ErrorCodeModule {}
