import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionDetectorDirective } from './position-detector.directive';

@NgModule({
  declarations: [PositionDetectorDirective],
  exports: [PositionDetectorDirective],
  imports: [CommonModule]
})
export class PositionDetectorModule {}
