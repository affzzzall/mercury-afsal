import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {
  // set compact class
  @Input() @HostBinding('class.compact') compact = false;

  constructor() {}

  ngOnInit(): void {}
}
