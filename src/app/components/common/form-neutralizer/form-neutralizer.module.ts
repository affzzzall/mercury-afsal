import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormNeutralizerDirective } from './form-neutralizer.directive';

@NgModule({
  declarations: [FormNeutralizerDirective],
  exports: [FormNeutralizerDirective],
  imports: [CommonModule]
})
export class FormNeutralizerModule {}
