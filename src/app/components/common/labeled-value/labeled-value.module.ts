import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LabeledValueComponent } from './labeled-value.component';

@NgModule({
  declarations: [LabeledValueComponent],
  exports: [LabeledValueComponent],
  imports: [CommonModule]
})
export class LabeledValueModule {}
