import { Component, ElementRef, HostBinding, HostListener, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppHorizontalPosition, AppVerticalPosition } from '../../position-detector/position-detector.directive';
import { FieldBaseComponent } from '../common/field-base.component';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent extends FieldBaseComponent<Date> {
  // set form control
  @Input() set control(formControl: FormControl) {
    if (formControl) {
      this.innerControl = formControl;
    }
  }
  // placeholder
  @Input() placeholder = '';
  // date format to display
  @Input() format = 'MM/dd/yyyy';
  // min date
  @Input() minDate: Date;
  // max date
  @Input() maxDate: Date;
  // disabled state
  @Input() disabled = false;
  // set error class
  @HostBinding('class.app-error') get error() {
    return this.innerControl.touched && this.innerControl.invalid;
  }
  // form control
  innerControl: FormControl = new FormControl();
  // opened state
  opened = false;
  // focus state
  focus = false;
  // vertical
  vertical: AppVerticalPosition = 'bottom';
  // horizontal
  horizontal: AppHorizontalPosition = 'right';

  constructor(public elementRef: ElementRef<HTMLElement>) {
    super();
  }

  /**
   * return value
   */
  get value() {
    return this.innerControl.value || null;
  }

  /**
   * update value of control when current date changed
   * @param date changed date
   */
  onCurrentDateChange(date: Date) {
    this.innerControl.setValue(date);
    this.closeCalendar();
  }

  /**
   * open calendar
   */
  openCalendar() {
    if (!this.disabled) {
      this.opened = true;
    }
  }

  /**
   * close calendar
   */
  closeCalendar() {
    if (!this.disabled) {
      this.opened = false;
      this.focus = false;
    }
  }

  /**
   * toggle calendar
   */
  toggleCalendar() {
    if (this.opened) {
      this.closeCalendar();
    } else {
      this.openCalendar();
    }
  }

  /**
   * listen mouse over
   */
  @HostListener('mouseover')
  onMouseOver() {
    this.focus = true;
  }

  /**
   * listen mouse out
   */
  @HostListener('mouseout')
  onMouseOut() {
    this.focus = false;
  }
}
