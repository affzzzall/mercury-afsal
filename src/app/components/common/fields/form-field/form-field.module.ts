import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormFieldComponent } from './form-field.component';
import { InputModule } from '../input/input.module';
import { SelectModule } from '../select/select.module';
import { FieldErrorModule } from '../../materialized-fields/field-error/field-error.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [FormFieldComponent],
  exports: [FormFieldComponent],
  imports: [CommonModule, InputModule, SelectModule, FieldErrorModule, ReactiveFormsModule, NgxMaskModule.forRoot()]
})
export class FormFieldModule {}
