import { Component, ElementRef, HostBinding, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { OptionItem } from '../../../../models/common/option-item';
import { SubscriptionInventory } from '../../../../utils/subscribe.util';
import { AppHorizontalPosition, AppVerticalPosition } from '../../position-detector/position-detector.directive';
import { FieldBaseComponent } from '../common/field-base.component';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent<T = string> extends FieldBaseComponent<T> implements OnInit, OnDestroy {
  // set select options
  @Input() set options(options: OptionItem<T>[]) {
    this.selectOptions = options || [];

    if (this.innerControl) {
      this.setSelectedOption(this.innerControl.value);
    }
  }
  // set form control
  @Input() set control(formControl: FormControl) {
    if (formControl) {
      this.innerControl = formControl;
      this.setSelectedOption(this.innerControl.value);
      this.subscribeControlChanges();
    }
  }
  // placeholder
  @Input() placeholder = '';
  // set disabled state
  @Input() disabled = false;
  // set error class
  @HostBinding('class.app-error') get error() {
    return this.innerControl.touched && this.innerControl.invalid;
  }
  // focus state
  focus = false;
  // opened state
  opened = false;
  // form control
  innerControl: FormControl = new FormControl();
  // select options
  selectOptions: OptionItem<T>[] = [];
  // selected option
  selectedOption: OptionItem<T>;
  // vertical position of options
  vertical: AppVerticalPosition = 'bottom';
  // horizontal position of options
  horizontal: AppHorizontalPosition = 'right';

  // subscription inventory
  private inventory: SubscriptionInventory = new SubscriptionInventory();
  // subscription keys
  private keys = {
    controlChanges: 'controlChanges'
  };

  constructor(public elementRef: ElementRef<HTMLElement>) {
    super();
  }

  ngOnInit(): void {
    this.subscribeControlChanges();
  }

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * listen mouse over
   */
  @HostListener('mouseover')
  onMouseOver() {
    this.focus = true;
  }

  /**
   * listen mouse out
   */
  @HostListener('mouseout')
  onMouseOut() {
    this.focus = false;
  }

  /**
   * open options
   */
  openOptions() {
    if (!this.disabled) {
      this.opened = true;
    }
  }

  /**
   * close options
   */
  closeOptions() {
    if (!this.disabled) {
      this.opened = false;
      this.focus = false;
    }
  }

  /**
   * toggle options opened state
   */
  toggleOptions() {
    if (this.opened) {
      this.closeOptions();
    } else {
      this.openOptions();
    }
  }

  /**
   * return value of selected option
   */
  get value() {
    return this.selectedOption ? this.selectedOption.label : '';
  }

  /**
   * update selected option and control's value
   * @param option selected option
   */
  onOptionClicked(option: OptionItem<T>) {
    this.innerControl.setValue(option.value);
    this.closeOptions();
  }

  /**
   * subscribe control changes
   */
  private subscribeControlChanges() {
    this.inventory.unSubscribe(this.keys.controlChanges);

    const sub = this.innerControl.valueChanges.subscribe({
      next: (value) => {
        this.setSelectedOption(value);
      }
    });

    this.inventory.store(this.keys.controlChanges, sub);
  }

  /**
   * set selected option with value
   * @param value value
   */
  private setSelectedOption(value: T) {
    this.selectedOption = this.selectOptions.find((option) => option.value === value);
  }

  /**
   * return true when option is selected
   * @param option option to check
   */
  isSelected(option: OptionItem<T>) {
    return this.selectedOption === option;
  }
}
