import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectComponent } from './select.component';
import { InputModule } from '../input/input.module';
import { OutsideDetectorModule } from '../../outside-detector/outside-detector.module';
import { AutoScrollModule } from '../../auto-scroll/auto-scroll.module';
import { PositionDetectorModule } from '../../position-detector/position-detector.module';
import { IconArrowDropUpModule } from '../../icons/icon-arrow-drop-up/icon-arrow-drop-up.module';
import { IconArrowDropDownModule } from '../../icons/icon-arrow-drop-down/icon-arrow-drop-down.module';

@NgModule({
  declarations: [SelectComponent],
  exports: [SelectComponent],
  imports: [
    CommonModule,
    InputModule,
    OutsideDetectorModule,
    AutoScrollModule,
    PositionDetectorModule,
    IconArrowDropDownModule,
    IconArrowDropUpModule
  ]
})
export class SelectModule {}
