import { Directive, Input, TemplateRef } from '@angular/core';

/**
 * custom body has following contexts:
 * - column: TableColumn
 * - row: row data
 * - value: cell value
 */
@Directive({
  selector: '[appTableBodyCell]'
})
export class TableBodyCellDirective {
  // column property
  @Input() columnProperty: string;

  constructor(public templateRef: TemplateRef<HTMLElement>) {}
}
