import { Directive, Input, TemplateRef } from '@angular/core';

/**
 * custom header has following contexts:
 * - column: TableColumn
 */
@Directive({
  selector: '[appTableHeaderCell]'
})
export class TableHeaderCellDirective {
  // column property
  @Input() columnProperty: string;

  constructor(public templateRef: TemplateRef<HTMLElement>) {}
}
