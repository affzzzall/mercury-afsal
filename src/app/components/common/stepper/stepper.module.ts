import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepperComponent } from './stepper.component';
import { IconCheckModule } from '../icons/icon-check/icon-check.module';

@NgModule({
  declarations: [StepperComponent],
  exports: [StepperComponent],
  imports: [CommonModule, IconCheckModule]
})
export class StepperModule {}
