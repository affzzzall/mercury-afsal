import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermissionsComponent } from './permissions.component';
import { CheckboxModule } from '../checkbox/checkbox.module';

@NgModule({
  declarations: [PermissionsComponent],
  exports: [PermissionsComponent],
  imports: [CommonModule, CheckboxModule]
})
export class PermissionsModule {}
