import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconLockSmallComponent } from './icon-lock-small.component';

describe('IconLockSmallComponent', () => {
  let component: IconLockSmallComponent;
  let fixture: ComponentFixture<IconLockSmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconLockSmallComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconLockSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
