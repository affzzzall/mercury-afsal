import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconLockSmallComponent } from './icon-lock-small.component';

@NgModule({
  declarations: [IconLockSmallComponent],
  exports: [IconLockSmallComponent],
  imports: [CommonModule]
})
export class IconLockSmallModule {}
