import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconArrowRightComponent } from './icon-arrow-right.component';

@NgModule({
  declarations: [IconArrowRightComponent],
  exports: [IconArrowRightComponent],
  imports: [CommonModule]
})
export class IconArrowRightModule {}
