import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconKeyComponent } from './icon-key.component';

@NgModule({
  declarations: [IconKeyComponent],
  exports: [IconKeyComponent],
  imports: [CommonModule]
})
export class IconKeyModule {}
