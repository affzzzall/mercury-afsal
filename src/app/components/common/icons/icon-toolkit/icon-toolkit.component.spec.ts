import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconToolkitComponent } from './icon-toolkit.component';

describe('IconToolkitComponent', () => {
  let component: IconToolkitComponent;
  let fixture: ComponentFixture<IconToolkitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconToolkitComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconToolkitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
