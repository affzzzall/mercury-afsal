import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconToolkitComponent } from './icon-toolkit.component';

@NgModule({
  declarations: [IconToolkitComponent],
  exports: [IconToolkitComponent],
  imports: [CommonModule]
})
export class IconToolkitModule {}
