import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconHelpComponent } from './icon-help.component';

describe('IconHelpComponent', () => {
  let component: IconHelpComponent;
  let fixture: ComponentFixture<IconHelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconHelpComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
