import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-help',
  templateUrl: './icon-help.component.html',
  styleUrls: ['./icon-help.component.scss']
})
export class IconHelpComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
