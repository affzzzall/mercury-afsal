import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconCalendarTodayComponent } from './icon-calendar-today.component';

@NgModule({
  declarations: [IconCalendarTodayComponent],
  exports: [IconCalendarTodayComponent],
  imports: [CommonModule]
})
export class IconCalendarTodayModule {}
