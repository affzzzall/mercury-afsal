import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconCalendarTodayComponent } from './icon-calendar-today.component';

describe('IconCalendarTodayComponent', () => {
  let component: IconCalendarTodayComponent;
  let fixture: ComponentFixture<IconCalendarTodayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconCalendarTodayComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconCalendarTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
