import { Component } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-calendar-today',
  templateUrl: './icon-calendar-today.component.html',
  styleUrls: ['./icon-calendar-today.component.scss']
})
export class IconCalendarTodayComponent extends IconBaseComponent {}
