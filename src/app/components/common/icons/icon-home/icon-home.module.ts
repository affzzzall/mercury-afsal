import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconHomeComponent } from './icon-home.component';

@NgModule({
  declarations: [IconHomeComponent],
  exports: [IconHomeComponent],
  imports: [CommonModule]
})
export class IconHomeModule {}
