import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconToolkitReverseComponent } from './icon-toolkit-reverse.component';

@NgModule({
  declarations: [IconToolkitReverseComponent],
  exports: [IconToolkitReverseComponent],
  imports: [CommonModule]
})
export class IconToolkitReverseModule {}
