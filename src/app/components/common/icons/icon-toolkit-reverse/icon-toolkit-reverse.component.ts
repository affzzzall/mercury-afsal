import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-toolkit-reverse',
  templateUrl: './icon-toolkit-reverse.component.html',
  styleUrls: ['./icon-toolkit-reverse.component.scss']
})
export class IconToolkitReverseComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
