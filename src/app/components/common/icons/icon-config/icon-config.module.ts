import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconConfigComponent } from './icon-config.component';

@NgModule({
  declarations: [IconConfigComponent],
  exports: [IconConfigComponent],
  imports: [CommonModule]
})
export class IconConfigModule {}
