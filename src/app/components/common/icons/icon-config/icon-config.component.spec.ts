import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconConfigComponent } from './icon-config.component';

describe('IconConfigComponent', () => {
  let component: IconConfigComponent;
  let fixture: ComponentFixture<IconConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconConfigComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
