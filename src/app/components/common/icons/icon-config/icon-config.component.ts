import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-config',
  templateUrl: './icon-config.component.html',
  styleUrls: ['./icon-config.component.scss']
})
export class IconConfigComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
