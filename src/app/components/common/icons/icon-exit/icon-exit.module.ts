import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconExitComponent } from './icon-exit.component';

@NgModule({
  declarations: [IconExitComponent],
  exports: [IconExitComponent],
  imports: [CommonModule]
})
export class IconExitModule {}
