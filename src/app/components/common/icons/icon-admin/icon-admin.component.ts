import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-admin',
  templateUrl: './icon-admin.component.html',
  styleUrls: ['./icon-admin.component.scss']
})
export class IconAdminComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
