import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconReloadComponent } from './icon-reload.component';

@NgModule({
  declarations: [IconReloadComponent],
  exports: [IconReloadComponent],
  imports: [CommonModule]
})
export class IconReloadModule {}
