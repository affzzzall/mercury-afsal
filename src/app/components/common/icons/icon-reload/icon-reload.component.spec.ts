import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconReloadComponent } from './icon-reload.component';

describe('IconReloadComponent', () => {
  let component: IconReloadComponent;
  let fixture: ComponentFixture<IconReloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconReloadComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconReloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
