import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconChevronRightDoubleComponent } from './icon-chevron-right-double.component';

describe('IconChevronRightDoubleComponent', () => {
  let component: IconChevronRightDoubleComponent;
  let fixture: ComponentFixture<IconChevronRightDoubleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconChevronRightDoubleComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconChevronRightDoubleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
