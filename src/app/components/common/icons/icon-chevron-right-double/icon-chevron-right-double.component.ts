import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-chevron-right-double',
  templateUrl: './icon-chevron-right-double.component.html',
  styleUrls: ['./icon-chevron-right-double.component.scss']
})
export class IconChevronRightDoubleComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
