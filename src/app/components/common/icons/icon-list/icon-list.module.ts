import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconListComponent } from './icon-list.component';

@NgModule({
  declarations: [IconListComponent],
  exports: [IconListComponent],
  imports: [CommonModule]
})
export class IconListModule {}
