import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-camera',
  templateUrl: './icon-camera.component.html',
  styleUrls: ['./icon-camera.component.scss']
})
export class IconCameraComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
