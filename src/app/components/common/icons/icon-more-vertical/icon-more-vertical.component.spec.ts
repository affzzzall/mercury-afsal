import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconMoreVerticalComponent } from './icon-more-vertical.component';

describe('IconMoreVerticalComponent', () => {
  let component: IconMoreVerticalComponent;
  let fixture: ComponentFixture<IconMoreVerticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconMoreVerticalComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconMoreVerticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
