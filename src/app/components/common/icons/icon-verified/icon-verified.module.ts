import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconVerifiedComponent } from './icon-verified.component';

@NgModule({
  declarations: [IconVerifiedComponent],
  exports: [IconVerifiedComponent],
  imports: [CommonModule]
})
export class IconVerifiedModule {}
