import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-verified',
  templateUrl: './icon-verified.component.html',
  styleUrls: ['./icon-verified.component.scss']
})
export class IconVerifiedComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
