import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconArrowLeftComponent } from './icon-arrow-left.component';

describe('IconArrowLeftComponent', () => {
  let component: IconArrowLeftComponent;
  let fixture: ComponentFixture<IconArrowLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconArrowLeftComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconArrowLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
