import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-arrow-left',
  templateUrl: './icon-arrow-left.component.html',
  styleUrls: ['./icon-arrow-left.component.scss']
})
export class IconArrowLeftComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
