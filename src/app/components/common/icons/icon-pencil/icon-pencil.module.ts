import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconPencilComponent } from './icon-pencil.component';

@NgModule({
  declarations: [IconPencilComponent],
  exports: [IconPencilComponent],
  imports: [CommonModule]
})
export class IconPencilModule {}
