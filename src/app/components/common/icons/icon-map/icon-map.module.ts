import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconMapComponent } from './icon-map.component';

@NgModule({
  declarations: [IconMapComponent],
  exports: [IconMapComponent],
  imports: [CommonModule]
})
export class IconMapModule {}
