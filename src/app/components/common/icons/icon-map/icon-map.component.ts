import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-map',
  templateUrl: './icon-map.component.html',
  styleUrls: ['./icon-map.component.scss']
})
export class IconMapComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
