import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconLockComponent } from './icon-lock.component';

describe('IconLockComponent', () => {
  let component: IconLockComponent;
  let fixture: ComponentFixture<IconLockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconLockComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconLockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
