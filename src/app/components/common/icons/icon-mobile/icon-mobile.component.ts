import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-mobile',
  templateUrl: './icon-mobile.component.html',
  styleUrls: ['./icon-mobile.component.scss']
})
export class IconMobileComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
