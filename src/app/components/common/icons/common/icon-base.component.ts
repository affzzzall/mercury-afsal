import { HostBinding } from '@angular/core';

export class IconBaseComponent {
  // icon base class
  @HostBinding('class.app-icon') class = true;
}
