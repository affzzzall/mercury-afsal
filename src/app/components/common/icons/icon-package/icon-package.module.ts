import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconPackageComponent } from './icon-package.component';

@NgModule({
  declarations: [IconPackageComponent],
  exports: [IconPackageComponent],
  imports: [CommonModule]
})
export class IconPackageModule {}
