import { NgModule } from '@angular/core';
import { IconArrowDropDownComponent } from './icon-arrow-drop-down.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [IconArrowDropDownComponent],
  exports: [IconArrowDropDownComponent],
  imports: [CommonModule]
})
export class IconArrowDropDownModule {}
