import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconArrowDropDownComponent } from './icon-arrow-drop-down.component';

describe('IconArrowDropDownComponent', () => {
  let component: IconArrowDropDownComponent;
  let fixture: ComponentFixture<IconArrowDropDownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconArrowDropDownComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconArrowDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
