import { Component } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-arrow-drop-down',
  templateUrl: './icon-arrow-drop-down.component.html',
  styleUrls: ['./icon-arrow-drop-down.component.scss']
})
export class IconArrowDropDownComponent extends IconBaseComponent {}
