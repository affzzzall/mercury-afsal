import { Component } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-chevron-left',
  templateUrl: './icon-chevron-left.component.html',
  styleUrls: ['./icon-chevron-left.component.scss']
})
export class IconChevronLeftComponent extends IconBaseComponent {}
