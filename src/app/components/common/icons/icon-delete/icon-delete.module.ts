import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconDeleteComponent } from './icon-delete.component';

@NgModule({
  declarations: [IconDeleteComponent],
  exports: [IconDeleteComponent],
  imports: [CommonModule]
})
export class IconDeleteModule {}
