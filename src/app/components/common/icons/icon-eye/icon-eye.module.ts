import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconEyeComponent } from './icon-eye.component';

@NgModule({
  declarations: [IconEyeComponent],
  exports: [IconEyeComponent],
  imports: [CommonModule]
})
export class IconEyeModule {}
