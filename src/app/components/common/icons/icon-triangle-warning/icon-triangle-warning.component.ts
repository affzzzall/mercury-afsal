import { Component, OnInit } from '@angular/core';
import { IconBaseComponent } from '../common/icon-base.component';

@Component({
  selector: 'app-icon-triangle-warning',
  templateUrl: './icon-triangle-warning.component.html',
  styleUrls: ['./icon-triangle-warning.component.scss']
})
export class IconTriangleWarningComponent extends IconBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
