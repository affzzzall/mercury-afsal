import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconTriangleWarningComponent } from './icon-triangle-warning.component';

@NgModule({
  declarations: [IconTriangleWarningComponent],
  exports: [IconTriangleWarningComponent],
  imports: [CommonModule]
})
export class IconTriangleWarningModule {}
