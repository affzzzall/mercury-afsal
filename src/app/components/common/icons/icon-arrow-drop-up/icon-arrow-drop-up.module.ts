import { NgModule } from '@angular/core';
import { IconArrowDropUpComponent } from './icon-arrow-drop-up.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [IconArrowDropUpComponent],
  exports: [IconArrowDropUpComponent],
  imports: [CommonModule]
})
export class IconArrowDropUpModule {}
