import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconArrowDropUpComponent } from './icon-arrow-drop-up.component';

describe('IconArrowDropUpComponent', () => {
  let component: IconArrowDropUpComponent;
  let fixture: ComponentFixture<IconArrowDropUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconArrowDropUpComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconArrowDropUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
