import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconUserCardComponent } from './icon-user-card.component';

@NgModule({
  declarations: [IconUserCardComponent],
  exports: [IconUserCardComponent],
  imports: [CommonModule]
})
export class IconUserCardModule {}
