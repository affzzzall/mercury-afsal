import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableSummaryComponent } from './table-summary.component';
import { IconReloadModule } from '../icons/icon-reload/icon-reload.module';
import { IconArrowDropUpModule } from '../icons/icon-arrow-drop-up/icon-arrow-drop-up.module';
import { IconArrowDropDownModule } from '../icons/icon-arrow-drop-down/icon-arrow-drop-down.module';

@NgModule({
  declarations: [TableSummaryComponent],
  exports: [TableSummaryComponent],
  imports: [CommonModule, IconReloadModule, IconArrowDropDownModule, IconArrowDropUpModule]
})
export class TableSummaryModule {}
