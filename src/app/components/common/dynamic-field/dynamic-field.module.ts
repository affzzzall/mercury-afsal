import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFieldComponent } from './dynamic-field.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from '../fields/select/select.module';
import { DateModule } from '../fields/date/date.module';

@NgModule({
  declarations: [DynamicFieldComponent],
  exports: [DynamicFieldComponent],
  imports: [CommonModule, ReactiveFormsModule, SelectModule, DateModule]
})
export class DynamicFieldModule {}
