import { Component, Input, OnInit } from '@angular/core';
import { AppUser } from '../../../models/data/app-user';

@Component({
  selector: 'app-user-avatar',
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.scss']
})
export class UserAvatarComponent implements OnInit {
  // user
  @Input() user: AppUser;

  constructor() {}

  ngOnInit(): void {}

  /**
   * get user initial
   */
  get userInitial() {
    const first = this.user.firstName[0].toUpperCase();
    const last = this.user.lastName ? this.user.lastName[0].toUpperCase() : null;

    if (last) {
      return first + last;
    } else {
      return first;
    }
  }
}
