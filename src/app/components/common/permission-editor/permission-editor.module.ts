import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermissionEditorComponent } from './permission-editor.component';
import { IconUserCardModule } from '../icons/icon-user-card/icon-user-card.module';
import { IconArrowDropUpModule } from '../icons/icon-arrow-drop-up/icon-arrow-drop-up.module';
import { IconArrowDropDownModule } from '../icons/icon-arrow-drop-down/icon-arrow-drop-down.module';
import { CardModule } from '../card/card.module';
import { OutsideDetectorModule } from '../outside-detector/outside-detector.module';
import { PositionDetectorModule } from '../position-detector/position-detector.module';
import { PermissionsModule } from '../permissions/permissions.module';

@NgModule({
  declarations: [PermissionEditorComponent],
  exports: [PermissionEditorComponent],
  imports: [
    CommonModule,
    IconUserCardModule,
    IconArrowDropUpModule,
    IconArrowDropDownModule,
    CardModule,
    OutsideDetectorModule,
    PositionDetectorModule,
    PermissionsModule
  ]
})
export class PermissionEditorModule {}
