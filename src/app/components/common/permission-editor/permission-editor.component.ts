import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { AppUserPermissions } from '../../../models/data/app-user-permissions';
import { AppUser } from '../../../models/data/app-user';
import { AppVerticalPosition } from '../position-detector/position-detector.directive';

@Component({
  selector: 'app-permission-editor',
  templateUrl: './permission-editor.component.html',
  styleUrls: ['./permission-editor.component.scss']
})
export class PermissionEditorComponent implements OnInit {
  // user
  @Input() user: AppUser;
  // permissions
  @Input() permissions: AppUserPermissions;

  // opened state
  opened = false;
  // options vertical position
  vertical: AppVerticalPosition = 'bottom';

  constructor(public elementRef: ElementRef<HTMLElement>) {}

  ngOnInit(): void {}

  /**
   * open
   */
  open() {
    this.opened = true;
  }

  /**
   * close
   */
  close() {
    this.opened = false;
  }

  /**
   * toggle opened
   */
  toggle() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }
}
