import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisabledReasonComponent } from './disabled-reason.component';

describe('DisabledReasonComponent', () => {
  let component: DisabledReasonComponent;
  let fixture: ComponentFixture<DisabledReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DisabledReasonComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisabledReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
