import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './calendar.component';
import { IconChevronRightModule } from '../icons/icon-chevron-right/icon-chevron-right.module';
import { IconChevronLeftModule } from '../icons/icon-chevron-left/icon-chevron-left.module';

@NgModule({
  declarations: [CalendarComponent],
  exports: [CalendarComponent],
  imports: [CommonModule, IconChevronRightModule, IconChevronLeftModule]
})
export class CalendarModule {}
