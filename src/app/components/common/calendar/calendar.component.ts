import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { getWeekDayString } from '../../../utils/date.util';
import { changeDetected } from '../../../utils/detector.util';
import { toInteger } from '../../../utils/format.util';
import { CalendarDate } from '../../../models/common/calendar-date';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, OnChanges {
  // set start day of calendar
  @Input() set calendarDay(day: string | number) {
    if (day !== undefined && day !== null) {
      this.startDay = toInteger(day);
      this.startDay = this.startDay > 6 ? this.startDay % 7 : this.startDay;
    }
  }
  // set calendar display date
  @Input() set calendarDate(date: { year: number; month: number }) {
    if (date) {
      const { year, month } = date;

      this.displayDate = new CalendarDate(year, month);
    }
  }
  // set date value
  @Input() set date(date: Date) {
    if (date) {
      this.currentDate = CalendarDate.fromDate(date);
      this.displayDate = this.currentDate;
    }
  }
  // set minimum date
  @Input() set minDate(minDate: Date) {
    if (minDate) {
      this.minimumDate = CalendarDate.fromDate(minDate);
    }
  }
  // set maximum date
  @Input() set maxDate(maxDate: Date) {
    if (maxDate) {
      this.maximumDate = CalendarDate.fromDate(maxDate);
    }
  }
  // start year of calendar
  @Input() startYear: number;
  // end year of calendar
  @Input() endYear: number;
  // set disabled state
  @Input() disabled = false;
  // date change emitter
  @Output() dateChange: EventEmitter<Date> = new EventEmitter<Date>();
  // it's different with display date
  currentDate: CalendarDate = new CalendarDate();
  // minimum date
  minimumDate: CalendarDate;
  // maximum date
  maximumDate: CalendarDate;
  // week day string array
  weekDayString: string[] = [];
  // displayable dates
  dates: CalendarDate[] = [];
  // today
  todayDate: CalendarDate = new CalendarDate();
  // display specific month of calendar
  // as default, calendar will display the month which contains today
  displayDate: CalendarDate = new CalendarDate();
  // start day of calendar
  // default is sunday
  startDay = 0;

  ngOnInit(): void {
    // create week days on init
    this.setWeekDayString();
    // create dates on init
    this.setDates();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // detect startDay change and update week day strings
    if (changeDetected(changes.startDay)) {
      this.setWeekDayString();
      this.setDates();
    }
  }

  /**
   * set week day string by start day
   */
  private setWeekDayString() {
    const days = getWeekDayString();
    let index = this.startDay || 0;

    this.weekDayString = days.map(() => {
      const day = days[index];

      index = index < days.length - 1 ? index + 1 : 0;

      return day;
    });
  }

  /**
   * set dates to display
   */
  private setDates() {
    // clear dates
    this.dates = [];

    const { year, month } = this.displayDate;
    const displayStartDay = new CalendarDate(year, month, 1).day;
    // set start index
    let startIndex = -displayStartDay + 1 + this.startDay;

    // if start index over 1, subtract one week
    startIndex = startIndex > 1 ? startIndex - 7 : startIndex;

    // create dates
    for (let i = startIndex; i < startIndex + 42; i++) {
      const date = new CalendarDate(year, month, i);

      this.dates.push(date);
    }
  }

  /**
   * return true when date is out of display date
   * @param date date to check
   */
  isOutOfDisplayDate(date: CalendarDate) {
    return (
      date.year !== this.displayDate.year ||
      date.month !== this.displayDate.month ||
      this.beforeMinimumDate(date) ||
      this.afterMaximumDate(date)
    );
  }

  /**
   * go to prev month
   */
  toPrevMonth() {
    const { year, month } = this.displayDate;

    if (!this.previousMonthDisabled) {
      this.setDisplayDate(year, month - 1);
    }
  }

  /**
   * go to next month
   */
  toNextMonth() {
    const { year, month } = this.displayDate;

    if (!this.nextMonthDisabled) {
      this.setDisplayDate(year, month + 1);
    }
  }

  /**
   * go to today date
   */
  toToday() {
    const { year, month } = this.todayDate;

    this.setDisplayDate(year, month);
  }

  /**
   * set display date with year and month
   * @param year year to display
   * @param month month to display
   */
  private setDisplayDate(year: number, month: number) {
    const next = new CalendarDate(year, month, 1);

    if ((this.endYear ? next.year <= this.endYear : true) && (this.startYear ? next.year >= this.startYear : true)) {
      this.displayDate = next;
    }

    this.setDates();
  }

  /**
   * handle click date item
   * @param item clicked date
   */
  onClickDate(item: CalendarDate) {
    if (!this.disabled && !this.beforeMinimumDate(item) && !this.afterMaximumDate(item)) {
      if (this.displayDate.year !== item.year || this.displayDate.month !== item.month) {
        this.setDisplayDate(item.year, item.month);
      }

      this.dateChange.emit(item.instance);
    }
  }

  /**
   * return true when date is before than minimum date
   * @param date calendar date
   */
  beforeMinimumDate(date: CalendarDate) {
    if (this.minimumDate) {
      return date.isBeforeThan(this.minimumDate);
    }
  }

  /**
   * return true when date is after than maximum date
   * @param date calendar date
   */
  afterMaximumDate(date: CalendarDate) {
    if (this.maximumDate) {
      return date.isAfterThan(this.maximumDate);
    }
  }

  /**
   * return true when previous month is disabled
   */
  get previousMonthDisabled() {
    const { year, month } = this.displayDate;

    return this.beforeMinimumDate(CalendarDate.fromDate(new Date(year, month - 1, 0)));
  }

  /**
   * return true when next month is disabled
   */
  get nextMonthDisabled() {
    const { year, month } = this.displayDate;

    return this.afterMaximumDate(CalendarDate.fromDate(new Date(year, month, 1)));
  }
}
