import { AfterContentInit, Directive, ElementRef, EventEmitter, Inject, Input, OnDestroy, Output } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { getElement } from '../../../utils/element.util';

@Directive({
  selector: '[appOutsideDetector]'
})
export class OutsideDetectorDirective implements AfterContentInit, OnDestroy {
  // set false to ignore click detection
  @Input() detectable = true;
  // container for outside detector
  @Input() outsideDetectorContainer: ElementRef | HTMLElement;
  // outside click emitter
  @Output() outsideClick: EventEmitter<void> = new EventEmitter<void>();
  // inside click emitter
  @Output() insideClick: EventEmitter<void> = new EventEmitter<void>();

  constructor(@Inject(DOCUMENT) private document: Document, private elementRef: ElementRef) {
    this.detectClick = this.detectClick.bind(this);
  }

  /**
   * add event to document on content init
   */
  ngAfterContentInit(): void {
    if (this.document) {
      this.document.addEventListener('click', this.detectClick, { capture: true });
    }
  }

  /**
   * remove event from document on destroy
   */
  ngOnDestroy(): void {
    if (this.document) {
      this.document.removeEventListener('click', this.detectClick, { capture: true });
    }
  }

  /**
   * detect click event
   * @param event event
   */
  detectClick(event: MouseEvent) {
    const target = this.outsideDetectorContainer || this.elementRef;

    if (target && this.detectable) {
      const container: HTMLElement = target instanceof ElementRef ? getElement(target) : target;

      if (container.contains(event.target as HTMLElement)) {
        this.insideClick.emit();
      } else {
        this.outsideClick.emit();
      }
    }
  }
}
