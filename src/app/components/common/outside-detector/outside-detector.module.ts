import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutsideDetectorDirective } from './outside-detector.directive';

@NgModule({
  declarations: [OutsideDetectorDirective],
  exports: [OutsideDetectorDirective],
  imports: [CommonModule]
})
export class OutsideDetectorModule {}
