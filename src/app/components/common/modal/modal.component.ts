import { Component } from '@angular/core';
import { ModalBaseComponent } from './common/modal-base.component';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent<T> extends ModalBaseComponent<T> {}
