import { EventEmitter, Output } from '@angular/core';

export class ModalBaseComponent<T> {
  // close click emitter
  @Output() closeClick: EventEmitter<void> = new EventEmitter<void>();
  // confirm click emitter
  @Output() confirmClick: EventEmitter<T> = new EventEmitter<T>();

  /**
   * emit closeClick emitter
   */
  onCloseClicked() {
    this.closeClick.emit();
  }
}
