import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { AppUser } from '../../../models/data/app-user';

export type AppUserInfoViewType = 'role' | 'email' | 'key' | 'custom';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  // user
  @Input() user: AppUser;
  // view type
  @Input() type: AppUserInfoViewType;
  // user name clickable state
  @Input() clickable = false;
  // tooltip visible state
  @Input() @HostBinding('class.app-tooltip-visible') tooltipVisible = false;
  // user name click emitter
  @Output() nameClick: EventEmitter<AppUser> = new EventEmitter<AppUser>();

  constructor() {}

  ngOnInit(): void {}

  /**
   * emit name click
   */
  emitNameClick() {
    if (this.clickable) {
      this.nameClick.emit(this.user);
    }
  }
}
