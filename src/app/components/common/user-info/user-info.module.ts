import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserInfoComponent } from './user-info.component';
import { UserAvatarModule } from '../user-avatar/user-avatar.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [UserInfoComponent],
  exports: [UserInfoComponent],
  imports: [CommonModule, UserAvatarModule, TooltipModule]
})
export class UserInfoModule {}
