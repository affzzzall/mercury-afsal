import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageListComponent } from './message-list/message-list.component';
import { MessageItemComponent } from './message-item/message-item.component';
import { MessageService } from './services/message.service';
import { IconVerifiedModule } from '../icons/icon-verified/icon-verified.module';

@NgModule({
  declarations: [MessageListComponent, MessageItemComponent],
  imports: [CommonModule, IconVerifiedModule],
  providers: [MessageService],
  exports: [MessageListComponent]
})
export class MessageModule {}
