import { Component, EventEmitter, HostBinding, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MessageType } from '../services/message.service';

@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.scss']
})
export class MessageItemComponent implements OnInit, OnDestroy {
  // message type
  @Input() @HostBinding('attr.app-type') type: MessageType;
  // undoable state
  @Input() undoable = false;
  // on undo
  @Input() onUndo: () => void;
  // destroy emitter
  @Output() destroy: EventEmitter<void> = new EventEmitter<void>();
  // seconds
  seconds = 6;
  // timer
  private timer;

  ngOnInit(): void {
    this.timer = setInterval(() => {
      this.seconds--;

      if (this.seconds < 1) {
        this.destroy.emit();
      }
    }, 1000);
  }

  ngOnDestroy(): void {
    clearTimeout(this.timer);
  }

  @HostListener('click')
  onClick() {
    this.destroy.emit();
  }

  /**
   * undo
   */
  onUndoClicked() {
    if (this.undoable && this.onUndo) {
      this.onUndo();
      this.destroy.emit();
    }
  }
}
