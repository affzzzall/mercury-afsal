import { Component } from '@angular/core';
import { MessageItem, MessageService } from '../services/message.service';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss'],
  animations: [
    trigger('message', [
      state(
        'void',
        style({
          opacity: 0,
          transform: 'translateY(100%)'
        })
      ),
      transition('void => *', [animate('.1s')]),
      transition('* => void', [animate('.1s')])
    ])
  ]
})
export class MessageListComponent {
  constructor(public messageService: MessageService) {}

  /**
   * close message item
   * @param item item
   */
  close(item: MessageItem) {
    this.messageService.close(item);
  }
}
