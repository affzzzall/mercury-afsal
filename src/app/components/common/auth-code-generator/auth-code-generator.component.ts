import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AppUser } from '../../../models/data/app-user';
import { UserService } from '../../../services/user.service';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../utils/subscribe.util';
import { MessageService } from '../message/services/message.service';
import { Subscription } from 'rxjs';

export type AppAuthCodeType = 'keyholder' | 'toolkit';

interface AuthCodeGeneratorSubscriptions extends SubscriptionKeys {
  generate: SubscriptionItem;
  send: SubscriptionItem;
}

@Component({
  selector: 'app-auth-code-generator',
  templateUrl: './auth-code-generator.component.html',
  styleUrls: ['./auth-code-generator.component.scss']
})
export class AuthCodeGeneratorComponent implements OnInit, OnDestroy {
  // set previous auth code
  @Input() set authCode(code: string) {
    this.code = code;
  }
  // user
  @Input() user: AppUser;
  // auth code generating type
  @Input() type: AppAuthCodeType;
  // can regenerate code
  @Input() canRegenerate = false;
  // code generated
  @Output() codeGenerated: EventEmitter<string> = new EventEmitter<string>();
  // loading state
  loading = false;
  // sending state
  sending = false;
  // generated code
  code: string;
  // inventory
  private inventory: SubscriptionInventory<AuthCodeGeneratorSubscriptions> = new SubscriptionInventory<
    AuthCodeGeneratorSubscriptions
  >();

  constructor(private userService: UserService, private messageService: MessageService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * generate code
   */
  generateCode() {
    if (!this.loading) {
      let sub: Subscription;

      switch (this.type) {
        case 'keyholder': {
          sub = this.generateKeyholderAuthCode();
          break;
        }

        case 'toolkit': {
          sub = this.generateToolkitAuthCode();
          break;
        }
      }

      if (sub) {
        this.inventory.unSubscribe('generate');
        this.inventory.store('generate', sub);
        this.loading = true;
      }
    }
  }

  /**
   * generate toolkit auth code
   */
  private generateToolkitAuthCode() {
    return this.userService.createToolkitAuthCode(this.user.email).subscribe({
      next: (code) => {
        this.loading = false;
        this.code = code;
        this.codeGenerated.emit(code);
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });
  }

  /**
   * generate keyholder auth code
   */
  private generateKeyholderAuthCode() {
    return this.userService.createKeyholderAuthCode(this.user.email).subscribe({
      next: (code) => {
        this.loading = false;
        this.code = code;
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });
  }

  /**
   * send auth code
   */
  sendAuthCode() {
    if (!this.sending) {
      this.inventory.unSubscribe('send');
      this.sending = true;

      const sub = this.userService.sendAuthCode(this.user.email, this.code).subscribe({
        next: () => {
          this.messageService.open('default', 'Auth code has been sent');
          this.sending = false;
        }
      });

      this.inventory.store('send', sub);
    }
  }
}
