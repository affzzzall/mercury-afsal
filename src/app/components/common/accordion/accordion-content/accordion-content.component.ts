import { Component, ElementRef, Input } from '@angular/core';
import { AccordionBaseComponent } from '../common/accordion-base.component';

@Component({
  selector: 'app-accordion-content',
  templateUrl: './accordion-content.component.html',
  styleUrls: ['./accordion-content.component.scss']
})
export class AccordionContentComponent extends AccordionBaseComponent {
  constructor(protected elementRef: ElementRef<HTMLElement>) {
    super(elementRef);
  }
}
