import { ElementRef } from '@angular/core';

export class AccordionBaseComponent {
  constructor(protected elementRef: ElementRef<HTMLElement>) {}

  /**
   * return header height
   */
  get height() {
    if (this.elementRef && this.elementRef.nativeElement) {
      return this.elementRef.nativeElement.offsetHeight;
    } else {
      return 0;
    }
  }
}
