import { AfterViewInit, Component, ContentChild, HostBinding, HostListener, OnDestroy } from '@angular/core';
import { AccordionHeaderComponent } from './accordion-header/accordion-header.component';
import { AccordionHeaderAdditionalComponent } from './accordion-header-additional/accordion-header-additional.component';
import { AccordionContentComponent } from './accordion-content/accordion-content.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
  animations: [
    trigger('expand', [
      state(
        'collapsed',
        style({
          height: '{{headerHeight}}px'
        }),
        {
          params: {
            headerHeight: 0
          }
        }
      ),
      state(
        'expanded',
        style({
          height: '{{totalHeight}}px'
        }),
        {
          params: {
            totalHeight: 0
          }
        }
      ),
      transition('expanded <=> collapsed', animate('.15s ease-out'))
    ])
  ]
})
export class AccordionComponent implements AfterViewInit, OnDestroy {
  // header
  @ContentChild(AccordionHeaderComponent) accordionHeader: AccordionHeaderComponent;
  // header additional
  @ContentChild(AccordionHeaderAdditionalComponent) accordionHeaderAdditional: AccordionHeaderAdditionalComponent;
  // content
  @ContentChild(AccordionContentComponent) accordionContent: AccordionContentComponent;
  // animation
  @HostBinding('@expand') get animation() {
    return {
      value: this.state,
      params: {
        headerHeight: this.headerHeight,
        totalHeight: this.headerHeight + this.contentHeight
      }
    };
  }

  // expanded state
  expanded = false;
  // subscription
  private subscription: Subscription = new Subscription();

  ngAfterViewInit(): void {
    this.subscribeHeaderClick();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * subscribe for header click
   */
  private subscribeHeaderClick() {
    if (this.accordionHeader) {
      const sub = this.accordionHeader.headerClick.subscribe(() => {
        this.toggle();
      });

      this.subscription.add(sub);
    }
  }

  /**
   * get header height with additional
   */
  get headerHeight() {
    let height = 0;

    if (this.accordionHeader) {
      height += this.accordionHeader.height;
    }

    if (this.accordionHeaderAdditional) {
      height += this.accordionHeaderAdditional.height;
    }

    return height;
  }

  /**
   * return content height
   */
  get contentHeight() {
    return this.accordionContent ? this.accordionContent.height : 0;
  }

  /**
   * return animation state
   */
  get state() {
    return this.expanded ? 'expanded' : 'collapsed';
  }

  /**
   * expand accordion
   */
  expand() {
    this.expanded = true;
    this.setHeaderExpanded();
  }

  /**
   * collapse accordion
   */
  collapse() {
    this.expanded = false;
    this.setHeaderExpanded();
  }

  /**
   * toggle state
   */
  toggle() {
    if (this.expanded) {
      this.collapse();
    } else {
      this.expand();
    }
  }

  /**
   * set header expanded state
   */
  private setHeaderExpanded() {
    if (this.accordionHeader) {
      this.accordionHeader.expanded = this.expanded;
    }
  }
}
