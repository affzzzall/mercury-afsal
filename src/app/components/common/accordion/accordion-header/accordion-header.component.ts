import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { AccordionBaseComponent } from '../common/accordion-base.component';

@Component({
  selector: 'app-accordion-header',
  templateUrl: './accordion-header.component.html',
  styleUrls: ['./accordion-header.component.scss']
})
export class AccordionHeaderComponent extends AccordionBaseComponent {
  // expanded state
  @Input() expanded = false;
  // header click emitter
  @Output() headerClick: EventEmitter<void> = new EventEmitter<void>();

  constructor(protected elementRef: ElementRef<HTMLElement>) {
    super(elementRef);
  }

  @HostListener('click')
  onClick() {
    this.headerClick.emit();
  }
}
