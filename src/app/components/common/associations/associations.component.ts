import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppVerticalPosition } from '../position-detector/position-detector.directive';
import { AppAssociationType } from '../../../models/data/app-associations';

export type AppAssociationsType = 'device' | 'space' | 'keyholder';

@Component({
  selector: 'app-associations',
  templateUrl: './associations.component.html',
  styleUrls: ['./associations.component.scss']
})
export class AssociationsComponent implements OnInit {
  // association type
  @Input() type: AppAssociationsType;
  // associations
  @Input() associations: AppAssociationType[] = [];
  // add click
  @Output() addClick: EventEmitter<void> = new EventEmitter<void>();
  // read only state
  @Input() readOnly = false;
  // opened state
  opened = false;
  // vertical position
  vertical: AppVerticalPosition = 'bottom';
  typeCheck: boolean;
  constructor(public elementRef: ElementRef<HTMLElement>) {}

  ngOnInit(): void {
    this.typeChecking();
  }

  /**
   * open
   */
  open() {
    this.opened = true;
  }

  /**
   * close
   */
  close() {
    this.opened = false;
  }

  /**
   * toggle opened
   */
  toggle() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }

  /**
   * emit add click
   */
  emitAddClick() {
    this.close();
    this.addClick.emit();
  }

  typeChecking() {
    if (this.type === 'device') {
      this.typeCheck = true;
    } else if (this.type === 'space') {
      this.typeCheck = false;
    }
  }
}
