import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appAutoScrollItem]'
})
export class AutoScrollItemDirective {
  // focused state
  @Input() focused = false;

  constructor(public elementRef: ElementRef<HTMLElement>) {}
}
