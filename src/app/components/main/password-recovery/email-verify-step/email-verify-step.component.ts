import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { UserService } from '../../../../services/user.service';
import { AppUser } from '../../../../models/data/app-user';
import { SubscriptionInventory, SubscriptionItem, SubscriptionKeys } from '../../../../utils/subscribe.util';
import { MessageService } from '../../../common/message/services/message.service';

export interface EmailVerifyStepSubscriptions extends SubscriptionKeys {
  verifyEmail: SubscriptionItem;
}

@Component({
  selector: 'app-email-verify-step',
  templateUrl: './email-verify-step.component.html',
  styleUrls: ['../common/password-recovery-base.component.scss', './email-verify-step.component.scss']
})
export class EmailVerifyStepComponent implements OnInit, OnDestroy {
  // verified emitter
  @Output() verified: EventEmitter<AppUser> = new EventEmitter<AppUser>();
  // email
  email: FormControl = new FormControl('', [Validators.required, Validators.email]);
  // email errors
  emailErrors = {
    required: 'The email is required',
    email: 'The email you entered is incorrect'
  };
  // not a robot checked state
  notARobot = false;
  // loading
  loading = false;
  // inventory
  private inventory: SubscriptionInventory<EmailVerifyStepSubscriptions> = new SubscriptionInventory<
    EmailVerifyStepSubscriptions
  >();

  constructor(private userService: UserService, private messageService: MessageService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * toggle not a robot checked state
   */
  toggleNotARobot() {
    this.notARobot = !this.notARobot;
  }

  /**
   * verify user email
   */
  verifyUserEmail() {
    // check validation
    this.email.markAsTouched();

    if (this.email.invalid) {
      return;
    }

    if (!this.notARobot) {
      this.messageService.open('error', 'Please check not a robot');
      return;
    }

    this.inventory.unSubscribe('verifyEmail');
    this.loading = true;

    const sub = this.userService.getUserByEmail(this.email.value).subscribe({
      next: (user) => {
        this.emitVerified(user);
      },
      error: (err) => {
        this.messageService.open('error', err.message);
        this.loading = false;
      }
    });

    this.inventory.store('verifyEmail', sub);
  }

  /**
   * emit verified
   * @param user verified user
   */
  private emitVerified(user: AppUser) {
    this.verified.emit(user);
  }
}
