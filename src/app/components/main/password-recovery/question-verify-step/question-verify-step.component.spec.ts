import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionVerifyStepComponent } from './question-verify-step.component';

describe('QuestionVerifyStepComponent', () => {
  let component: QuestionVerifyStepComponent;
  let fixture: ComponentFixture<QuestionVerifyStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuestionVerifyStepComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionVerifyStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
