import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { SubscriptionInventory } from '../../../../utils/subscribe.util';
import { Router } from '@angular/router';
import { ToggleService } from 'src/app/services/common/toggle.service';

@Component({
  selector: 'app-recovery-email-sent',
  templateUrl: './recovery-email-sent.component.html',
  styleUrls: ['../common/password-recovery-base.component.scss', './recovery-email-sent.component.scss']
})
export class RecoveryEmailSentComponent implements OnInit, OnDestroy {
  // user email
  @Input() email: string;
  // loading
  loading = false;

  private inventory: SubscriptionInventory = new SubscriptionInventory();

  constructor(private router: Router, private userService: UserService, private toggleService: ToggleService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.inventory.unSubscribeAll();
  }

  /**
   * resend email
   */
  resendEmail() {
    this.inventory.unSubscribe('resend');

    this.loading = true;

    const sub = this.userService.resendRecoveryEmail(this.email).subscribe({
      next: () => {
        this.loading = false;
      }
    });

    this.inventory.store('resend', sub);
  }
  redirectLogin() {
    this.toggleService.loginSub.next(true);
    this.router.navigate(['/main/login']);
  }
}
