/**
 * get random number within specific range
 * @param min min number
 * @param max max number
 */
export function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

/**
 * pick random item from array
 * @param items random item candidates
 */
export function randomPick(items) {
  return items[randomNumber(0, items.length)];
}

/**
 * return random key
 */
export function randomKey() {
  return Math.random().toString(32).split('.')[1];
}

/**
 * return random date
 */
export function randomDate() {
  const start = new Date(1990, 1, 1);
  const end = new Date();

  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}
