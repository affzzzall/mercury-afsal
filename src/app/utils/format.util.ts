/**
 * value to integer number
 * @param value value to format
 */
export function toInteger(value: string | number) {
  if (typeof value === 'string') {
    return parseInt(value, null);
  } else {
    return value;
  }
}

/**
 * add prefixed zero to number
 * @param value value to format
 */
export function addZero(value: string | number) {
  value = toInteger(value);

  return value < 10 ? `0${value}` : `${value}`;
}
