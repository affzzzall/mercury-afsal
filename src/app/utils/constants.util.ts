// user role
export const ADMIN_ROLE = 'Administrator';
export const KEYHOLDER_ROLE = 'Keyholder';

// states
// admin
export const ADMIN_ACTIVE_STATE = 'Active';
export const ADMIN_INACTIVE_STATE = 'Inactive';
export const ADMIN_INVITED_STATE = 'Invited';
export const ADMIN_DISABLED_STATE = 'Disabled';
// keyholder
export const KEYHOLDER_ACTIVE_STATE = 'Active';
export const KEYHOLDER_INACTIVE_STATE = 'Inactive';
export const KEYHOLDER_DISABLED_STATE = 'Disabled';
// device
export const DEVICE_ACTIVE_STATE = 'Active';
export const DEVICE_INACTIVE_STATE = 'Inactive';
export const DEVICE_DISABLED_STATE = 'Disabled';
export const DEVICE_LOW_BATTERY_STATE = 'Low Battery';
export const DEVICE_UNREGISTERED_STATE = 'Unregistered';
export const DEVICE_TAMPERED_STATE = 'Tampered';
export const DEVICE_ERROR_STATE = 'Error';
// space
export const SPACE_ACTIVE_STATE = 'Active';
export const SPACE_INACTIVE_STATE = 'Inactive';
export const SPACE_DISABLED_STATE = 'Disabled';
// access history
export const ACCESS_HISTORY_SUCCESS_STATE = 'Success';
export const ACCESS_HISTORY_ERROR_STATE = 'Error';

// colors for state
export const ADMIN_ACTIVE_COLOR = '#B5B5B5';
export const ADMIN_INVITED_COLOR = '#7EA7FF';
export const ADMIN_INACTIVE_COLOR = '#1B4298';
export const ADMIN_DISABLED_COLOR = '#E5EAF3';

// colors for keyholder
export const KEYHOLDER_ACTIVE_COLOR = '#B5B5B5';
export const KEYHOLDER_INACTIVE_COLOR = '#7EA7FF';
export const KEYHOLDER_DISABLED_COLOR = '#1B4298';

// colors for devices
export const DEVICE_ACTIVE_COLOR = '#B5B5B5';
export const DEVICE_INACTIVE_COLOR = '#7EA7FF';
export const DEVICE_DISABLED_COLOR = '#5580DD';
export const DEVICE_LOW_BATTERY_COLOR = '#FFCA00';
export const DEVICE_UNREGISTERED_COLOR = '#1B4298';
export const DEVICE_TAMPERED_COLOR = '#E5EAF3';
export const DEVICE_ERROR_COLOR = '#3E3E3E';

// colors for spaces
export const SPACE_ACTIVE_COLOR = '#B5B5B5';
export const SPACE_INACTIVE_COLOR = '#7EA7FF';
export const SPACE_DISABLED_COLOR = '#1B4298';
