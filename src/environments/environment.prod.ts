export const environment = {
  production: true,
  api: {
    host: '/assets/json',
    delay: 500
  }
};
