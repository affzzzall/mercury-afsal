const {
  randomPick,
  randomFirstName,
  randomLastName,
  randomPinNumber,
  randomDate,
  randomNumber,
  randomActivity
} = require('./random.util');

const { writeFileSync } = require('fs');

const { join } = require('path');

const { locations } = require('./create-mock-data-locations');

// json directory
const jsonDirectory = join(__dirname, '../src/assets/json');
// disabled reason
const disabledReason = 'The Administrator becomes disable because lorem ipsum dolor sit amet disable description.';

function getAssociatedDevices() {
  const data = [];
  const size = randomNumber(2, 17);
  const startDeviceSn = 10382;

  for (let i = 0; i < size; i++) {
    const state = randomPick(['Active', 'Unregistered', 'Low Battery', 'Disabled', 'Inactive']);
    const status = randomPick(['Occupied', 'Available', 'Needs Maintenance', 'Paid']);

    const device = {
      id: 'D0' + (startDeviceSn + i),
      sn: randomPick(['D0' + (startDeviceSn + i), null]),
      name: randomPick([
        'Main Door',
        'Side Door',
        'Garage Door',
        'Garage 1',
        "Office's Floor South Door",
        'Warehouse',
        'Garage West Side Back Door',
        "Main Office's 2nd Floor Door"
      ]),
      state,
      status,
      createdAt: randomDate(new Date(2020, 0, 1), new Date(2020, 3, 30))
    };

    data.push(device);
  }

  return data;
}

function getAssociatedSpaces() {
  const data = [];
  const size = randomNumber(2, 17);
  const startSpaceId = 30489;

  for (let i = 0; i < size; i++) {
    const state = randomPick(['Active', 'Inactive', 'Disabled']);
    const status = randomPick(['Paid', 'Available', 'Rented']);

    const space = {
      id: 'S0' + (startSpaceId + i),
      name: randomPick([
        "Mike's House",
        "John's House",
        "Lukas's House",
        "Ryan's Office West Campus",
        "John's Office South Campus",
        "John's Office West Campus",
        "John's Office North Campus",
        "George's Office North Campus",
        "George's Office West Campus",
        "George's Office South Campus",
        "George's Room",
        "John's Room",
        "Lukas's Room"
      ]),
      state,
      status,
      createdAt: randomDate(new Date(2020, 0, 1), new Date(2020, 3, 30))
    };

    data.push(space);
  }

  return data;
}

function getAssociatedKeyholders() {
  const data = [];
  const size = randomNumber(2, 17);

  const startUserId = 23040;
  const startKeySn = 30981;

  for (let i = 0; i < size; i++) {
    // random names
    const firstName = randomFirstName();
    const lastName = randomLastName();

    // key state
    const state = randomPick(['Active', 'Inactive', 'Disabled']);
    const status = randomPick(['Paid', 'Unpaid', null]);

    const user = {
      id: 'A0' + (startUserId + i),
      role: 'Keyholder',
      name: `${firstName} ${lastName}`,
      firstName,
      lastName,
      email: `${firstName.toLowerCase()}_${lastName.toLowerCase()}@example.com`,
      password: 'password',
      avatar: null,
      location: randomPick(locations),
      question: "What's my first cat's name?",
      answer: 'answer',
      state,
      status,
      disabledReason: state === 'Disabled' ? disabledReason : null,
      createdAt: randomDate(new Date(2020, 0, 1), new Date(2020, 3, 30)),
      key: {
        sn: 'K0' + (startKeySn + i),
        pin: randomPinNumber()
      }
    };

    data.push(user);
  }

  return data;
}

function getActivityHistories() {
  const size = 30;
  const data = [];

  for (let i = 0; i < size; i++) {
    const activity = {
      activity: randomActivity(),
      createdAt: new Date(2020, 3, 12, 14, 30)
    };

    data.push(activity);
  }

  data.sort((a, b) => {
    return b.createdAt.valueOf() - a.createdAt.valueOf();
  });

  return data;
}

function getAccessHistories() {
  const data = [];
  const size = 20;

  for (let i = 0; i < size; i++) {
    // state
    const state = randomPick(['Success', 'Error']);
    // error code
    const code = state === 'Error' ? randomPick(['9099', '904C', '90Bb']) : null;

    const accessHistory = {
      createdAt: randomDate(new Date(2020, 3, 1), new Date(2020, 3, 30)),
      device: randomPick(getAssociatedDevices()),
      keyholder: randomPick(getKeyholders()),
      location: randomPick(locations),
      operation: 'Open Device',
      state,
      errorInfo: {
        code,
        description: code
          ? `Error ${code} description lorem ipsum capitalize on low hanging fruit. to identify a ballpark value added activity to beta test.`
          : null,
        solution: code
          ? `To mitigate ${code} errors, issue a retry from the device. To automatically manage retries, make sure you use the latest version of the Mercury IoT SDKs. For best practice on transient fault handling and retries, see Transient fault handling. If the problem persists, check Resource Health and Mercury Status to see if IoT Hub has a known problem. You can also use the manual failover feature. If there are no known problems and the issue continues, <a class="app-link">contact support</a> for further investigation.`
          : null
      },
      map: randomPick([true, false])
    };

    data.push(accessHistory);
  }

  data.sort((a, b) => {
    return b.createdAt.valueOf() - a.createdAt.valueOf();
  });

  return data;
}

function createGetAdmins() {
  const data = [];
  const size = 100;

  const startUserId = 23041;
  const startToolkitSn = 40702;

  for (let i = 0; i < size; i++) {
    const state = randomPick(['Active', 'Inactive', 'Disabled', 'Invited']);
    const status = randomPick(['Primary', 'Backup']);

    // random names
    const firstName = randomFirstName();
    const lastName = randomLastName();

    const adminRead = randomPick([true, false]);
    const keyholderRead = randomPick([true, false]);
    const deviceRead = randomPick([true, false]);
    const spaceRead = randomPick([true, false]);
    const historyRead = randomPick([true, false]);
    const configurationRead = randomPick([true, false]);
    const hasToolKit = randomPick([true, false]);
    // toolkit info
    const toolkitInfo = {
      sn: 'T0' + (startToolkitSn + i),
      pin: randomPinNumber(),
      dns: '012345.908.mercury.com'
    };

    const user = {
      id: 'A0' + (startUserId + i),
      role: 'Administrator',
      firstName,
      lastName,
      email: `${firstName.toLowerCase()}_${lastName.toLowerCase()}@example.com`,
      password: 'password',
      avatar: null,
      question: "What's my first cat's name?",
      answer: 'answer',
      state,
      status,
      disabledReason: state === 'Disabled' ? disabledReason : null,
      permissions: {
        administrators: {
          read: adminRead,
          edit: adminRead ? randomPick([true, false]) : false
        },
        keyholders: {
          read: keyholderRead,
          edit: keyholderRead ? randomPick([true, false]) : false
        },
        devices: {
          read: deviceRead,
          edit: deviceRead ? randomPick([true, false]) : false
        },
        spaces: {
          read: spaceRead,
          edit: spaceRead ? randomPick([true, false]) : false
        },
        accessHistory: {
          read: historyRead,
          edit: historyRead ? randomPick([true, false]) : false
        },
        configurations: {
          read: configurationRead,
          edit: configurationRead ? randomPick([true, false]) : false
        }
      },
      toolkit: hasToolKit ? toolkitInfo : null,
      createdAt: randomDate(new Date(2020, 0, 1), new Date(2020, 3, 30)),
      recentActivity: getActivityHistories()[0]
    };

    data.push(user);
  }

  writeFileSync(join(jsonDirectory, '/users/get-admins.json'), JSON.stringify(data, null, 2));
}

function getKeyholders() {
  const data = [];
  const size = 100;

  const startUserId = 23040;
  const startKeySn = 30981;

  for (let i = 0; i < size; i++) {
    // random names
    const firstName = randomFirstName();
    const lastName = randomLastName();

    // key state
    const state = randomPick(['Active', 'Inactive', 'Disabled']);
    const status = randomPick(['Paid', 'Unpaid', null]);

    const user = {
      id: 'A0' + (startUserId + i),
      role: 'Keyholder',
      firstName,
      lastName,
      email: `${firstName.toLowerCase()}_${lastName.toLowerCase()}@example.com`,
      password: 'password',
      avatar: null,
      location: randomPick(locations),
      question: "What's my first cat's name?",
      answer: 'answer',
      state,
      status,
      disabledReason: state === 'Disabled' ? disabledReason : null,
      createdAt: randomDate(new Date(2020, 0, 1), new Date(2020, 3, 30)),
      key: {
        sn: 'K0' + (startKeySn + i),
        pin: randomPinNumber()
      },
      associatedDevices: getAssociatedDevices(),
      associatedSpaces: getAssociatedSpaces()
    };

    data.push(user);
  }

  return data;
}

function createGetKeyholders() {
  const data = getKeyholders();

  writeFileSync(join(jsonDirectory, '/users/get-keyholders.json'), JSON.stringify(data, null, 2));
}

function createGetAccessHistories() {
  const data = getAccessHistories();

  writeFileSync(join(jsonDirectory, '/access-histories/get-access-histories.json'), JSON.stringify(data, null, 2));
}

function getDevices() {
  // devices
  const data = [];
  const size = 100;
  // device sn suffix number
  const startDeviceSn = 42076;

  // create devices
  for (let i = 0; i < size; i++) {
    const state = randomPick(['Active', 'Unregistered', 'Low Battery', 'Disabled', 'Inactive']);
    const status = randomPick(['Occupied', 'Available', 'Needs Maintenance', 'Paid']);

    const device = {
      id: 'D0' + (startDeviceSn + i),
      sn: randomPick(['D0' + (startDeviceSn + i), null]),
      name: randomPick([
        'Main Door',
        'Side Door',
        'Garage Door',
        'Garage 1',
        "Office's Floor South Door",
        'Warehouse',
        'Garage West Side Back Door',
        "Main Office's 2nd Floor Door"
      ]),
      state,
      status,
      associatedSpaces: getAssociatedSpaces(),
      lastAccess: randomPick([randomPick(getAccessHistories()), null])
    };

    data.push(device);
  }

  return data;
}

function createGetDevices() {
  const data = getDevices();

  writeFileSync(join(jsonDirectory, '/devices/get-devices.json'), JSON.stringify(data, null, 2));
}

function createGetActivityHistories() {
  const data = getActivityHistories();

  writeFileSync(join(jsonDirectory, '/activity-histories/get-activity-histories.json'), JSON.stringify(data, null, 2));
}

function createUnassociatedDevices() {
  const data = [];
  const size = 30;
  const startDeviceSn = 10382;

  for (let i = 0; i < size; i++) {
    const state = randomPick(['Active', 'Unregistered', 'Low Battery', 'Disabled', 'Inactive']);
    const status = randomPick(['Occupied', 'Available', 'Needs Maintenance', 'Paid']);

    const device = {
      id: 'D0' + (startDeviceSn + i),
      sn: randomPick(['D0' + (startDeviceSn + i), null]),
      name: randomPick([
        'Main Door',
        'Side Door',
        'Garage Door',
        'Garage 1',
        "Office's Floor South Door",
        'Warehouse',
        'Garage West Side Back Door',
        "Main Office's 2nd Floor Door"
      ]),
      state,
      status,
      createdAt: randomDate(new Date(2020, 0, 1), new Date(2020, 3, 30))
    };

    data.push(device);
  }

  writeFileSync(join(jsonDirectory, '/devices/get-unassociated-devices.json'), JSON.stringify(data, null, 2));
}

function createUnassociatedKeyholders() {
  const data = [];
  const size = 30;

  const startUserId = 23040;
  const startKeySn = 30981;

  for (let i = 0; i < size; i++) {
    // random names
    const firstName = randomFirstName();
    const lastName = randomLastName();

    // key state
    const state = randomPick(['Active', 'Inactive', 'Disabled']);
    const status = randomPick(['Paid', 'Unpaid', null]);

    const user = {
      id: 'A0' + (startUserId + i),
      role: 'Keyholder',
      name: `${firstName} ${lastName}`,
      firstName,
      lastName,
      email: `${firstName.toLowerCase()}_${lastName.toLowerCase()}@example.com`,
      password: 'password',
      avatar: null,
      location: randomPick(locations),
      question: "What's my first cat's name?",
      answer: 'answer',
      state,
      status,
      disabledReason: state === 'Disabled' ? disabledReason : null,
      createdAt: randomDate(new Date(2020, 0, 1), new Date(2020, 3, 30)),
      key: {
        sn: 'K0' + (startKeySn + i),
        pin: randomPinNumber()
      }
    };

    data.push(user);
  }

  writeFileSync(join(jsonDirectory, '/users/get-unassociated-keyholders.json'), JSON.stringify(data, null, 2));
}

function createUnassociatedSpaces() {
  const data = [];
  const size = 30;
  const startSpaceId = 30489;

  for (let i = 0; i < size; i++) {
    const state = randomPick(['Active', 'Inactive', 'Disabled']);
    const status = randomPick(['Paid', 'Available', 'Rented']);

    const space = {
      id: 'S0' + (startSpaceId + i),
      name: randomPick([
        "Mike's House",
        "John's House",
        "Lukas's House",
        "Ryan's Office West Campus",
        "John's Office South Campus",
        "John's Office West Campus",
        "John's Office North Campus",
        "George's Office North Campus",
        "George's Office West Campus",
        "George's Office South Campus",
        "George's Room",
        "John's Room",
        "Lukas's Room"
      ]),
      state,
      status,
      createdAt: randomDate(new Date(2020, 0, 1), new Date(2020, 3, 30))
    };

    data.push(space);
  }

  writeFileSync(join(jsonDirectory, '/spaces/get-unassociated-spaces.json'), JSON.stringify(data, null, 2));
}

function getSpaces() {
  const data = [];
  const size = 100;
  const startSpaceId = 20398;

  for (let i = 0; i < size; i++) {
    const state = randomPick(['Active', 'Inactive', 'Disabled']);
    const status = randomPick(['Paid', 'Available', 'Rented']);

    const space = {
      id: 'S0' + (startSpaceId + i),
      name: randomPick([
        "Mike's House",
        "John's House",
        "Lukas's House",
        "Ryan's Office West Campus",
        "John's Office South Campus",
        "John's Office West Campus",
        "John's Office North Campus",
        "George's Office North Campus",
        "George's Office West Campus",
        "George's Office South Campus",
        "George's Room",
        "John's Room",
        "Lukas's Room"
      ]),
      state,
      status,
      location: randomPick(locations),
      associatedDevices: getAssociatedDevices(),
      associatedKeyholders: getAssociatedKeyholders(),
      lastAccess: randomPick([randomPick(getAccessHistories()), null])
    };

    data.push(space);
  }

  return data;
}

function createGetSpaces() {
  const data = getSpaces();

  writeFileSync(join(jsonDirectory, '/spaces/get-spaces.json'), JSON.stringify(data, null, 2));
}

function createGetStatus() {
  const description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque velit vitae viverra tincidunt. Integer urna nunc, consequat eget feugiat eget, rutrum quis magna. Maecenas lacinia vestibulum ex, eu sagittis diam. Curabitur leo ante, sollicitudin sit amet ultrices eu, faucibus vitae mi. Etiam ligula eros, elementum ac suscipit a, ornare sed lacus. Aenean dignissim`;

  const data = [
    {
      group: 'device',
      label: 'Occupied',
      description,
      default: true,
      use: true
    },
    {
      group: 'device',
      label: 'Available',
      description,
      default: false,
      use: true
    },
    {
      group: 'device',
      label: 'Needs Maintenance',
      description,
      default: false,
      use: true
    },
    {
      group: 'device',
      label: 'Paid',
      description,
      default: false,
      use: true
    },
    {
      group: 'device',
      label: 'Bad Performance',
      description,
      default: false,
      use: false
    },
    {
      group: 'admin',
      label: 'Primary',
      description,
      default: false,
      use: true
    },
    {
      group: 'admin',
      label: 'Backup',
      description,
      default: false,
      use: true
    },
    {
      group: 'keyholder',
      label: 'Paid',
      description,
      default: false,
      use: true
    },
    {
      group: 'keyholder',
      label: 'Unpaid',
      description,
      default: false,
      use: true
    },
    {
      group: 'space',
      label: 'Paid',
      description,
      default: false,
      use: true
    },
    {
      group: 'space',
      label: 'Available',
      description,
      default: false,
      use: true
    },
    {
      group: 'space',
      label: 'Rented',
      description,
      default: false,
      use: true
    }
  ];

  writeFileSync(join(jsonDirectory, '/statuses/get-statuses.json'), JSON.stringify(data, null, 2));
}

createGetAdmins();
createGetKeyholders();
createGetAccessHistories();
createGetActivityHistories();
createUnassociatedDevices();
createUnassociatedSpaces();
createUnassociatedKeyholders();
createGetDevices();
createGetSpaces();
createGetStatus();
