# Verification

## Login page

Go to [http://localhost:4200/main/login](http://localhost:4200/main/login)

### Cookie Message

Refresh the browser without doing anything.\
The cookie message will be displayed since the user didn't click the close icon.

Click the `Forgot password?` button.\
Then you can see the cookie message is keep displayed.

Return to Login page and click the `Register here` button.\
Then you can see the cookie message is hidden according to the design.\
The cookie message will be appeared when you go to the Login page or Password Recovery page.

Click the close icon in the cookie message and then refresh the browser.\
The cookie message will be disappeared.

### Remember me

Login with `mark_code@example.com` without checking Remember me.\
Then refresh the browser and you can see the application redirects to the login page.

Login with `mark_code@example.com` with checking Remember me.\
In this case, the application redirects to the `/dashboard/home` page if you refresh the browser.\
If you want to logout, click the Logout button from the top-right user menu.

## Password Recovery

Go to [http://localhost:4200/main/password-recovery](http://localhost:4200/main/password-recovery)

### Email Verification

Click Next button without doing anything.\
Then you can see the Email ID field highlighted with error message.

Input any email and click Next button without checking `I'm not a robot`.\
The error message will be appeared.

Input `mark_code@example.com` for the Email ID field and click Next button with checking `I'm not a robot`.\
Then you can see secret question page.\
Please notice that you can use any Email ID which is listed in `src/assets/users.json`.

### Secret Question

For all users, the answer for secret question is `answer`.

### Email Sent

From the Email sent page, you can simulate resend email by clicking `Recend email` button.

## Dashboard

You can get here by login with valid user.

### Location

All tables are reloaded when the location is changed.\
You can change location with the selector in the header.\
But please notice that the displaying data won't be changed since it's a prototype with mock data.

When you input some text from the location selector, it will filter the location with name or ID.

### Edit Profile

Uploading image for the avatar can change preview.\
But it won't be applied when you leave the Edit Profile page since the Done button is dummy for now.\
It was confirmed from the forum.\
Ref: [https://apps.topcoder.com/forums/?module=Thread&threadID=956969&start=0](https://apps.topcoder.com/forums/?module=Thread&threadID=956969&start=0)

#### Toolkit Configuration

You can see the toolkit configuration is hidden when you logged in with `lord_code@example.com` or `lord_johnson@example.com`.

### Tables

The pagination is working for the table.\
But sorting, filtering, reloading will just reload the data again.

### Add to a {xxx}

From the `Add to a {xxx}` page, the number of unassociated items is total number.\
So it won't be changed even if the user filter the list with search text.

### Admins Page

The access permission column in the admin table is read only.\
You can change the permission from the Admin Edit modal.

### Read Only

You can see read only pages when you logged in with `lord_code@example.com`.\
All editable actions are restricted.

### Unreadable

When you login with `lord_johnson@example.com`, the Admins, Keyholders, Devices, Spaces, and Config pages will be hidden.\
Since those pages are restricted with guards, you can't get to the pages even using the direct urls, too.\
When you checked Remember me from the login page, the application will be redirected to the `/dashboard/home`.\
If not, the application will be redirected to `/main/login`.
